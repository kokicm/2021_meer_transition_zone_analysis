# 2021_meer_transition_zone_analysis

- ImageJ macro to extract intensity profiles
- Extraced intensity profiles
- Julia script to analyze extracted intensity profiles and to create the figures

Update: Added extended Julia script to capture somite stages (_ss).

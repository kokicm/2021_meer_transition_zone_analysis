// Contour Profiles
// FIrst you have to create a freeline ROI
// This macro then creates and shifts duplicates of the ROI
// and then plots all intensity profiles. 

setAutoThreshold("Default dark");
  macro "Contour Profiles" {

width = 50 // ROI width in pixels

      for (i=0; i<width; i++) {
		roiManager("Select", i);
		roiManager("translate", i, 0); // -i for leftwards
		//getSelectionBounds(x, y, w, h);
		//setSelectionLocation(x+1, y);
		run("Create Selection");
		run("Restore Selection");
		roiManager("Add");
		roiManager("Show All");
      }

      	
run("ROI Manager...");
roiManager("Multi Plot");

  }

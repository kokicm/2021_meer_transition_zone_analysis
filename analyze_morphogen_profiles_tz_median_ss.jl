#=  
This script analyzes preprocessed data in the CSV format to perform statistics on mouse embryo neural tube expression domain boundary positions and their transition widths 
=#

ENV["GKS_ENCODING"]="utf-8"
# Load packages
using CSV,DataFrames,DataFramesMeta,Glob,Statistics,Plots,StatsPlots,Distances,Interpolations,LaTeXStrings,LsqFit,SpecialFunctions,IntervalSets,Distributions,Bootstrap,ColorSchemes,ColorBrewer,GRUtils,GLM;
gr() # Choose GR as plotting backend




# Functions, definitions, initialization


function get_gdf(path)
# get_gdf grabs all intensity profiles and creates a grouped dataframe (grouped for samples)

    files=glob("*.csv", path) #Vector of filenames. Glob allows you to use the asterisk.
    dfs = DataFrame.( CSV.File.( files; header=1))
    filenames = readdir(path)
    # add an indicator column and an ID column
    for i in 1:length(dfs)
        dfs[i][!, :sample] .=  i
        dfs[i][!,:batch_sample] .= parse(Int64,match(r"#\d{2}", filenames[i]).match[2:3])
        dfs[i][!, :ID] .= parse(Int64,match(r"\d{5}", filenames[i]).match)
        dfs[i][!, :subclass] .= match(r"\d{5}\w", filenames[i]).match[6]
    end

    df=reduce(vcat,dfs,cols = :union)
    df = coalesce.(df,0)
    df[:,1] = convert.(Float64, df[!,1])
 
    rename!(df, "Distance_(microns)" => :Distance)

    #= 
    Conversion to microns while accounting for different batch resolutions using batch IDs. 
        
        Weirdly enough, ImageJ version 1.53 stores freehand ROI measurements that are open curves not in microns, but in microns²/px when using 'Multi Plot' to store multiple intensity profiles. Thus, to convert to microns, conversion factors below have the unit px/micron.
    =#

    df[!,1] = map(eachrow(df)) do row
        if row.ID == 16452 ||  row.ID == 16457 ||  row.ID == 16449
            return row.Distance*1024.0/400.11
        elseif row.ID == 16547
            return row.Distance*1024.0/457.27
        elseif row.ID == 16550
            return row.Distance*1024.0/320.09
        elseif row.ID == 16553 || row.ID == 16540 || row.ID == 16544 || row.ID == 16551
            return row.Distance*1024.0/640.17
        elseif row.ID == 16453
            return row.Distance*1024.0/533.48
        else
            return row.Distance
        end
    end

    df[!,:ss] .= NaN

    df[!,:ss] = map(eachrow(df)) do row
        if row.ID == 16452 && ( row.batch_sample == 3 || row.batch_sample == 5 || row.batch_sample == 7)
            return row.ss = 24.
        elseif row.ID == 16452 && ( row.batch_sample == 2 || row.batch_sample == 4 || row.batch_sample == 6)
            return row.ss = 29.
        elseif row.ID == 16452 && ( row.batch_sample == 11 || row.batch_sample == 13 || row.batch_sample == 14)
            return row.ss = 25.
        elseif row.ID == 16457 && ( row.batch_sample == 1 || row.batch_sample == 3 || row.batch_sample == 6 || row.batch_sample == 8 || row.batch_sample == 10)
            return row.ss = 25.
        elseif row.ID == 16457 && ( row.batch_sample == 2 || row.batch_sample == 4 || row.batch_sample == 11 )
            return row.ss = 24.
        elseif row.ID == 16457 && ( row.batch_sample == 5 || row.batch_sample == 7 || row.batch_sample == 9 )
            return row.ss = 20.
        elseif row.ID == 16449 && ( row.batch_sample == 1 || row.batch_sample == 3 || row.batch_sample == 5 || row.batch_sample == 8 || row.batch_sample == 10 )
            return row.ss = 25.
        elseif row.ID == 16449 && ( row.batch_sample == 2 || row.batch_sample == 4 || row.batch_sample == 6 )
            return row.ss = 24.
        elseif row.ID == 16449 && ( row.batch_sample == 7 || row.batch_sample == 9 || row.batch_sample == 12 )
            return row.ss = 20.
        elseif row.ID == 16547 && ( row.batch_sample == 1 || row.batch_sample == 4 || row.batch_sample == 7 )
            return row.ss = 32.
        elseif row.ID == 16547 && ( row.batch_sample == 2 || row.batch_sample == 5 || row.batch_sample == 8 )
            return row.ss = 24.5
        elseif row.ID == 16547 && ( row.batch_sample == 3 || row.batch_sample == 6 || row.batch_sample == 9 )
            return row.ss = 28.5
        elseif row.ID == 16550 && ( row.batch_sample == 3 ||  row.batch_sample == 7 )
            return row.ss = 21.
        elseif row.ID == 16550 && ( row.batch_sample == 4 ||  row.batch_sample == 6 ||  row.batch_sample == 8)
            return row.ss = 25.
        elseif row.ID == 16550 && ( row.batch_sample == 1 ||  row.batch_sample == 2 )
            return row.ss = 23.
        elseif row.ID == 16540 && ( row.batch_sample == 3 || row.batch_sample == 6 || row.batch_sample == 9 || row.batch_sample == 12 )
            return row.ss = 35.
        elseif row.ID == 16540 && ( row.batch_sample == 1 || row.batch_sample == 4 || row.batch_sample == 7 || row.batch_sample == 10 || row.batch_sample == 12)
            return row.ss = 39.5
        elseif row.ID == 16540 && ( row.batch_sample == 2 || row.batch_sample == 5 || row.batch_sample == 8 || row.batch_sample == 11 )
            return row.ss = 36.5
        elseif row.ID == 16544 && ( row.batch_sample == 2 || row.batch_sample == 7 || row.batch_sample == 10 )
            return row.ss = 32.
        elseif row.ID == 16544 && ( row.batch_sample == 3 || row.batch_sample == 5 || row.batch_sample == 8 || row.batch_sample == 11 )
            return row.ss = 34.
        elseif row.ID == 16544 && ( row.batch_sample == 1 ||  row.batch_sample == 4 ||  row.batch_sample == 6)
            return row.ss = 35.
        elseif row.ID == 16453 && ( row.batch_sample == 1 || row.batch_sample == 3 || row.batch_sample == 5 || row.batch_sample == 8 || row.batch_sample == 6 || row.batch_sample == 9 || row.batch_sample == 10)
            return row.ss = 32.5
        elseif row.ID == 16453 && ( row.batch_sample == 2 || row.batch_sample == 4 || row.batch_sample == 7  )
            return row.ss = 33
        elseif row.ID == 16551 && ( 
                                    (row.batch_sample == 1 && row.subclass =='a') || 
                                    (row.batch_sample == 1 && row.subclass =='b') || 
                                    (row.batch_sample == 4 && row.subclass =='b') 
                                    )
            return row.ss = 48
        elseif row.ID == 16551 && ( 
                                    (row.batch_sample == 2 && row.subclass =='b') || 
                                    (row.batch_sample == 5 && row.subclass =='b')  
                                     )
            return row.ss = 46
        elseif row.ID == 16551 && ( 
                                     (row.batch_sample == 2 && row.subclass =='a') || 
                                     (row.batch_sample == 3 && row.subclass =='b') || 
                                     (row.batch_sample == 6 && row.subclass =='b')  
                                   )
            return row.ss = 48
        else
            return row.ss = NaN
        end
    end


    gdf = groupby(df,:sample)

    # calculate NT limit
    L_T = combine(gdf, 1 => maximum)
    L_T.L_F =(L_T[:,2].-100)./12.16
    df = innerjoin(df,L_T,on=:sample)

    # subtract floor- and roofplate diameters
    L = df[!,"Distance_maximum"] .- 2 .* df.L_F 

    intensities = select(df, r"Y")
    df.intensities = median(Matrix(intensities), dims=2)[:,1] # median intensity profile

    df = select!(df, ["Distance","sample","Distance_maximum","intensities","ss" ])

    df.L = L
    rename!(df,[:DVposition,:sample,:L_T,:intensities,:ss,:L])
    groupby(df,:sample)
end

# plot_single_profile creates a scatter plot that shows the fit, boundary position and transition width 
function plot_single_profile(g,lower_bound,upper_bound)
    xplot,yplot=Vector(g.DVposition),Vector(g.intensities);
    g= g[g.DVposition .>=lower_bound,:]
    g= g[g.DVposition .<=upper_bound,:]
    xdata,ydata=Vector(g.DVposition),Vector(g.intensities);
    Plots.scatter(xplot,yplot,label="pixel intensity")
    # fitting a nonlinear logistic function (Hill)
    @. H(x,p) = p[1] - (p[1]-p[2])/(1+ (x/p[3]  )^p[4]  )
    Hfit = LsqFit.curve_fit(H,xdata,ydata,[maximum(g.intensities),minimum(g.intensities),median(g.DVposition),1.],lower=[0.1,1e-5,0.1,-Inf])
    Plots.plot!(xdata,H(xdata,[coef(Hfit)[1],coef(Hfit)[2],coef(Hfit)[3],coef(Hfit)[4]]),label="Hill fit")
    IC50 = coef(Hfit)[3]
    vline!([IC50],label="boundary pos.="*string(round(IC50))*"μm")
    vline!([(10/(100-10))^(1/coef(Hfit)[4])*coef(Hfit)[3]],label="TZ upper bound",color=:red)
    vline!([(90/(100-90))^(1/coef(Hfit)[4])*coef(Hfit)[3]],label="TZ lower bound",color=:red)
end

# # Fit function (Hill)
# @. H(x,p) = p[1] - (p[1]-p[2])/(1+ (x/p[3]  )^p[4]  );

function get_L_T(g)
# just a getter for L_T
    maximum(g.L_T)
end

function calculate_TZ(g,lower_bound,upper_bound,TZ)
# calculate_TZ calculates the boundary position. Input params control the region to be fitted, required for optimal fitting.

    g= g[g.DVposition .>=lower_bound,:]
    g= g[g.DVposition .<=upper_bound,:]
        xdata,ydata=Vector(g.DVposition),Vector(g.intensities);
        # fitting a nonlinear logistic function (Hill)
        @. H(x,p) = p[1] - (p[1]-p[2])/(1+ (x/p[3]  )^p[4]  )
        Hfit = LsqFit.curve_fit(H,xdata,ydata,[maximum(g.intensities),minimum(g.intensities),median(g.DVposition),1.],lower=[0.1,1e-5,0.1,-Inf])
        df_intensities = DataFrame([xdata H(xdata,[coef(Hfit)[1],coef(Hfit)[2],coef(Hfit)[3],coef(Hfit)[4]])],:auto)
        tz_upper = (10/(100-10))^(1/coef(Hfit)[4])*coef(Hfit)[3]
        tz_lower = (90/(100-90))^(1/coef(Hfit)[4])*coef(Hfit)[3]
        rename!(df_intensities,[:DVposition,:intensities])
        append!(TZ, DataFrame(boundary = coef(Hfit)[3], L_T = maximum(g.L_T), tz_lower = tz_lower, tz_upper =tz_upper,lower_bound=lower_bound,upper_bound=upper_bound,ss=unique(g.ss)) )
end

macro name(arg)
# get name of var as string
    x = string(arg)
    quote
        $x
    end
end


function prep_df(df,name)
# prep_df preps the boundary position dataframe

    rename!(df,[:boundary,:L_T,:tz_lower,:tz_upper,:lower_bound,:upper_bound,:ss])
    df[!,:domain].=String(name)
    #rename!(df,[name,Symbol("tz_lower_",name),Symbol("tz_upper_",name)])

    df.sample = 1:nrow(df)
    df
end

function getSE(bsdata,estimator)
# get_SE calculates the standard error using bootstrapping

    n_boot = 10000 # resampling
    se(bootstrap(estimator, bsdata, BasicSampling(n_boot)))[1]
end




function binning(bins,DFtz)
# binning applies chosen binnings and bootstrapping of readout/boundary position stds to obtain standard errors

    # first create bins
    DFtz[!,:bins] .= NaN
    DFtz[!,:boundary_std] .= NaN
    for i in 1:length(bins)-1
        DFtz.bins[ (DFtz.L_T .>= bins[i]) .& (DFtz.L_T .< bins[i+1]) ] .= bins[i]
        DFtz.bins[ (DFtz.L_T .>= bins[length(bins)])  ] .= bins[length(bins)]
    end
    

    DFtz = filter(x->(ismissing(x.bins ) || !isnan(x.bins)), DFtz)

    DFtz[!,:n_measurements] .= NaN
    bDF = groupby(DFtz,:bins)
    for b in bDF
            b.n_measurements .= nrow(b)      
    end

    dDF = groupby(DataFrame(bDF),:domain)

    function domainStats(domainNumber)
        dsDF = combine(groupby(dDF[domainNumber],:bins), [:boundary,:L_T,:domain] => ( (p,s,t) -> (boundary_mean=mean(p), boundary_std=std(p), boundary_se= getSE(p,std), L_T=mean(s), domain=unique(t)[1] ) ) => AsTable)
        innerjoin(dsDF, unique( select!(DFtz, ["domain","n_measurements"])),on=:domain)
    end

    # perform stats on each domain 
    NKX22_V_std = domainStats(1)
    NKX22_D_std = domainStats(2) 
    OLIG2_V_std = domainStats(3) 
    OLIG2_D_std = domainStats(4) 
    NKX61_V_std = domainStats(5) 
    NKX61_D_std = domainStats(6) 
    OLIG3_V_std = domainStats(7) 
    PAX6_V_std  = domainStats(8)  
    PAX6_D_std  = domainStats(9)  

    vcat(NKX22_V_std,NKX22_D_std,OLIG2_V_std,OLIG2_D_std,NKX61_V_std,NKX61_D_std,PAX6_V_std,PAX6_D_std,OLIG3_V_std)
end

function binning_tz(bins)
# binning_tz applies chosen binnings and bootstrapping of transition width stds to calculate standard errors

    # first create bins
    DFtz[!,:bins] .= NaN
    for i in 1:length(bins)-1
        DFtz.bins[ (DFtz.L_T .>= bins[i]) .& (DFtz.L_T .< bins[i+1]) ] .= bins[i]
        DFtz.bins[ (DFtz.L_T .>= bins[length(bins)])  ] .= bins[length(bins)]
    end

    dDF = groupby(DFtz,:domain)

    # perform stats on each domain 
    NKX22_V =     combine(groupby(dDF[1],:bins), [:boundary,:tz, :L_T] => ((o,p, s) -> (boundary_mean=mean(o),boundary_std=std(o),boundary_se= getSE(o,std),tz_mean=mean(p), tz_se= getSE(p,mean),L_T=mean(s))) =>
    AsTable)
    NKX22_D =    combine(groupby(dDF[2],:bins), [:boundary,:tz, :L_T] => ((o,p, s) -> (boundary_mean=mean(o),boundary_std=std(o),boundary_se=  getSE(o,std),tz_mean=mean(p), tz_se= getSE(p,mean),L_T=mean(s))) =>
    AsTable)
    OLIG2_V =     combine(groupby(dDF[3],:bins), [:boundary,:tz, :L_T] => ((o,p, s) -> (boundary_mean=mean(o),boundary_std=std(o),boundary_se=  getSE(o,std),tz_mean=mean(p), tz_se= getSE(p,mean),L_T=mean(s))) =>
    AsTable)
    OLIG2_D =     combine(groupby(dDF[4],:bins), [:boundary,:tz, :L_T] => ((o,p, s) -> (boundary_mean=mean(o),boundary_std=std(o),boundary_se=  getSE(o,std),tz_mean=mean(p), tz_se= getSE(p,mean),L_T=mean(s))) =>
    AsTable)
    NKX61_V =    combine(groupby(dDF[5],:bins), [:boundary,:tz, :L_T] => ((o,p, s) -> (boundary_mean=mean(o),boundary_std=std(o),boundary_se=  getSE(o,std),tz_mean=mean(p), tz_se= getSE(p,mean),L_T=mean(s))) =>
    AsTable)
    NKX61_D =   combine(groupby(dDF[6],:bins), [:boundary,:tz, :L_T] => ((o,p, s) -> (boundary_mean=mean(o),boundary_std=std(o),boundary_se=  getSE(o,std),tz_mean=mean(p), tz_se= getSE(p,mean),L_T=mean(s))) =>
    AsTable)
    OLIG3_V =   combine(groupby(dDF[7],:bins), [:boundary,:tz, :L_T] => ((o,p, s) -> (boundary_mean=mean(o),boundary_std=std(o),boundary_se=  getSE(o,std),tz_mean=mean(p), tz_se= getSE(p,mean),L_T=mean(s))) =>
    AsTable)
    PAX6_V =    combine(groupby(dDF[8],:bins), [:boundary,:tz, :L_T] => ((o,p, s) -> (boundary_mean=mean(o),boundary_std=std(o),boundary_se=  getSE(o,std),tz_mean=mean(p), tz_se= getSE(p,mean),L_T=mean(s))) =>
    AsTable)
    PAX6_D =    combine(groupby(dDF[9],:bins), [:boundary,:tz, :L_T] => ((o,p, s) -> (boundary_mean=mean(o),boundary_std=std(o),boundary_se=  getSE(o,std),tz_mean=mean(p), tz_se=  getSE(p,mean),L_T=mean(s))) =>
    AsTable)

    # add domain name as column
    NKX22_V[!,:domain].=unique(dDF[1].domain)[1]
    NKX22_D[!,:domain].=unique(dDF[2].domain)[1]
    OLIG2_V[!,:domain].=unique(dDF[3].domain)[1]
    OLIG2_D[!,:domain].=unique(dDF[4].domain)[1]
    NKX61_V[!,:domain].=unique(dDF[5].domain)[1]
    NKX61_D[!,:domain].=unique(dDF[6].domain)[1]
    OLIG3_V[!,:domain].=unique(dDF[7].domain)[1]
    PAX6_V[!,:domain].=unique(dDF[8].domain)[1]
    PAX6_D[!,:domain].=unique(dDF[9].domain)[1]

    vcat(NKX22_V,NKX22_D,OLIG2_V,OLIG2_D,NKX61_V,NKX61_D,OLIG3_V,PAX6_V,PAX6_D)
end


function getFraction(stdDF,threshold)
#= 
getFraction calculates the fraction of readout/boundary position se 
≤ 4.9 μm (1 cell diameter) or 
≤ 9.8 μm (1 cell diameter)
=#

        dDF = groupby(stdDF,:domain)
        fDF = combine(x -> fraction=count(x.boundary_std.<=threshold)/length(x.boundary_std), dDF)
        rename!(fDF, :x1 => :fraction)
        DF = innerjoin(stdDF,fDF,on=:domain)
        unique( select!(DF, ["domain","fraction","bin_size"]))

end


function getTimeAv(intervals)
# temporal averaging of the se of boundary/readout positions

    stdDF = binning(intervals)
    stdDF = filter(x->(ismissing(x.boundary_std ) || !isnan(x.boundary_std)), stdDF)
    gstdDF = groupby(stdDF,:domain)
    combine(gstdDF, :boundary_std => mean)
end


function getstdDF(intervals)
# getter for binned+bootstrapped boundary/readout positions

    stdDF = binning(intervals,DFtz)
    filter(x->(ismissing(x.boundary_std ) || !isnan(x.boundary_std)), stdDF)
end


function getmeantzDF(intervals)
# getter for binned+bootstrapped transition widths

    meantzDF = binning_tz(intervals)
    filter(x->(ismissing(x.tz_mean ) || !isnan(x.tz_mean)), meantzDF)
end


function plot_L_T_vs_tz_mean(meantzDF,boolean)
# plot function for supp fig panels

    Plots.scatter(meantzDF.L_T,meantzDF.tz_mean,group=meantzDF.domain, yerror=meantzDF.tz_se,
        line=(:spline,2),
        yaxis=:log,
        xlims=(150,700),
        ylims=(1,1000),
        yticks=[0.1,1.,10.,100.,1000.],
            xlabel="NT length [μm]",
                ylabel="transition width [μm]",
                legend=boolean,
                color_palette=custom_colors,
                labels = permutedims(tickers)
        )
end


# workaround to reorder sequence of domain boundaries in legend by replacing domain boundary labels -->

tickers_new = [1,2,3,4,5,6,7,8,9]
tickers = ["NKX22_V","NKX22_D","OLIG2_V","OLIG2_D","NKX61_V","NKX61_D","PAX6_V","PAX6_D","OLIG3_V"]

function switch2int(domaincol)
    replace(domaincol,
        tickers[1] => tickers_new[1],
        tickers[2] => tickers_new[2],
        tickers[3] => tickers_new[3],
        tickers[4] => tickers_new[4],
        tickers[5] => tickers_new[5],
        tickers[6] => tickers_new[6],
        tickers[7] => tickers_new[7],
        tickers[8] => tickers_new[8],
        tickers[9] => tickers_new[9]
    )
end

function switch2str(domaincol)
    replace(domaincol,
        tickers_new[1] => tickers[1],
        tickers_new[2] => tickers[2],
        tickers_new[3] => tickers[3],
        tickers_new[4] => tickers[4],
        tickers_new[5] => tickers[5],
        tickers_new[6] => tickers[6],
        tickers_new[7] => tickers[7],
        tickers_new[8] => tickers[8],
        tickers_new[9] => tickers[9]
    )
end

# <--


# Define colors
greens = ColorBrewer.palette("Greens", 9);
blues = ColorBrewer.palette("Blues", 9);
yellows = ColorBrewer.palette("YlOrRd", 3);
custom_colors = [greens[3],greens[7],:magenta,:red,blues[3],blues[7],yellows[1],yellows[2],:brown]


# # # initialization of output objects

# NKX22_V   = [] |> DataFrame
# NKX22_D   = [] |> DataFrame
# NKX61_V   = [] |> DataFrame
# NKX61_D   = [] |> DataFrame
# OLIG2_V   = [] |> DataFrame
# OLIG2_D   = [] |> DataFrame
# OLIG3_V   = [] |> DataFrame
# PAX6_V    = [] |> DataFrame
# PAX6_D    = [] |> DataFrame

# # #= 
# # Now loading intensity profiles into grouped dataframes. 

# # Using relative paths, requires correct setting of working directory in IDE.
# # =#

# NKX22 = get_gdf(pwd()*raw"\intensity_profiles\NKX22")
# NKX61 = get_gdf(pwd()*raw"/intensity_profiles/NKX61")
# OLIG2 = get_gdf(pwd()*raw"/intensity_profiles/OLIG2")
# OLIG3 = get_gdf(pwd()*raw"/intensity_profiles/OLIG3")
# PAX6 = get_gdf(pwd()*raw"/intensity_profiles/PAX6")



# # Manual procedure to determine interval for fitting

# # NKX22_V

# # visual inspection, adjust last two parameters (interval)
# plot_single_profile(NKX22[1],0,10) 
# # copy adjusted parameters and execute to add results to initialized dataframe 
# calculate_TZ(NKX22[1],0,10,NKX22_V) 

# plot_single_profile(NKX22[2],10,50)
# calculate_TZ(NKX22[2],10,50,NKX22_V)

# plot_single_profile(NKX22[3],10,40)
# calculate_TZ(NKX22[3],10,40,NKX22_V)

# plot_single_profile(NKX22[4],0,50)
# calculate_TZ(NKX22[4],0,50,NKX22_V)

# plot_single_profile(NKX22[5],0,60)
# calculate_TZ(NKX22[5],0,60,NKX22_V)

# plot_single_profile(NKX22[6],0,50)
# calculate_TZ(NKX22[6],0,50,NKX22_V)

# plot_single_profile(NKX22[7],0,70)
# calculate_TZ(NKX22[7],0,70,NKX22_V)

# plot_single_profile(NKX22[8],0,60)
# calculate_TZ(NKX22[8],0,60,NKX22_V)

# plot_single_profile(NKX22[9],0,60)
# calculate_TZ(NKX22[9],0,60,NKX22_V)

# plot_single_profile(NKX22[10],0,70)
# calculate_TZ(NKX22[10],0,70,NKX22_V)

# plot_single_profile(NKX22[11],0,70)
# calculate_TZ(NKX22[11],0,70,NKX22_V)

# plot_single_profile(NKX22[12],0,90)
# calculate_TZ(NKX22[12],0,90,NKX22_V)

# plot_single_profile(NKX22[13],0,70)
# calculate_TZ(NKX22[13],0,70,NKX22_V)

# plot_single_profile(NKX22[14],0,60)
# calculate_TZ(NKX22[14],0,60,NKX22_V)

# plot_single_profile(NKX22[15],0,60)
# calculate_TZ(NKX22[15],0,60,NKX22_V)

# plot_single_profile(NKX22[16],0,50)
# calculate_TZ(NKX22[16],0,50,NKX22_V)

# plot_single_profile(NKX22[17],0,60)
# calculate_TZ(NKX22[17],0,60,NKX22_V)

# plot_single_profile(NKX22[18],0,70)
# calculate_TZ(NKX22[18],0,70,NKX22_V)

# plot_single_profile(NKX22[19],0,60)
# calculate_TZ(NKX22[19],0,60,NKX22_V)

# plot_single_profile(NKX22[20],0,60)
# calculate_TZ(NKX22[20],0,60,NKX22_V)

# plot_single_profile(NKX22[21],0,80)
# calculate_TZ(NKX22[21],0,80,NKX22_V)

# plot_single_profile(NKX22[22],0,70)
# calculate_TZ(NKX22[22],0,70,NKX22_V)

# plot_single_profile(NKX22[23],0,70)
# calculate_TZ(NKX22[23],0,70,NKX22_V)

# plot_single_profile(NKX22[24],0,80)
# calculate_TZ(NKX22[24],0,80,NKX22_V)

# plot_single_profile(NKX22[25],0,50)
# calculate_TZ(NKX22[25],0,50,NKX22_V)

# plot_single_profile(NKX22[26],0,70)
# calculate_TZ(NKX22[26],0,70,NKX22_V)

# plot_single_profile(NKX22[27],0,60)
# calculate_TZ(NKX22[27],0,60,NKX22_V)

# plot_single_profile(NKX22[28],0,65)
# calculate_TZ(NKX22[28],0,65,NKX22_V)

# plot_single_profile(NKX22[29],0,70)
# calculate_TZ(NKX22[29],0,70,NKX22_V)

# plot_single_profile(NKX22[30],0,75)
# calculate_TZ(NKX22[30],0,75,NKX22_V)

# plot_single_profile(NKX22[31],0,60)
# calculate_TZ(NKX22[31],0,60,NKX22_V)

# plot_single_profile(NKX22[32],0,60)
# calculate_TZ(NKX22[32],0,60,NKX22_V)

# plot_single_profile(NKX22[33],0,60)
# calculate_TZ(NKX22[33],0,60,NKX22_V)

# plot_single_profile(NKX22[34],0,60)
# calculate_TZ(NKX22[34],0,60,NKX22_V)

# plot_single_profile(NKX22[35],0,60)
# calculate_TZ(NKX22[35],0,60,NKX22_V)

# plot_single_profile(NKX22[36],0,60)
# calculate_TZ(NKX22[36],0,60,NKX22_V)

# plot_single_profile(NKX22[37],0,60)
# calculate_TZ(NKX22[37],0,60,NKX22_V)

# plot_single_profile(NKX22[38],0,65)
# calculate_TZ(NKX22[38],0,65,NKX22_V)

# plot_single_profile(NKX22[39],0,60)
# calculate_TZ(NKX22[39],0,60,NKX22_V)

# plot_single_profile(NKX22[40],0,65)
# calculate_TZ(NKX22[40],0,65,NKX22_V)

# plot_single_profile(NKX22[41],0,75)
# calculate_TZ(NKX22[41],0,75,NKX22_V)

# plot_single_profile(NKX22[42],0,75)
# calculate_TZ(NKX22[42],0,75,NKX22_V)

# plot_single_profile(NKX22[43],0,60)
# calculate_TZ(NKX22[43],0,60,NKX22_V)

# plot_single_profile(NKX22[44],0,60)
# calculate_TZ(NKX22[44],0,60,NKX22_V)

# plot_single_profile(NKX22[45],0,80)
# calculate_TZ(NKX22[45],0,80,NKX22_V)

# plot_single_profile(NKX22[46],0,60)
# calculate_TZ(NKX22[46],0,60,NKX22_V)

# plot_single_profile(NKX22[47],0,65)
# calculate_TZ(NKX22[47],0,65,NKX22_V)

# plot_single_profile(NKX22[48],0,60)
# calculate_TZ(NKX22[48],0,60,NKX22_V)

# plot_single_profile(NKX22[49],0,70)
# calculate_TZ(NKX22[49],0,70,NKX22_V)

# plot_single_profile(NKX22[50],0,70)
# calculate_TZ(NKX22[50],0,70,NKX22_V)

# plot_single_profile(NKX22[51],0,70)
# calculate_TZ(NKX22[51],0,70,NKX22_V)

# plot_single_profile(NKX22[52],0,65)
# calculate_TZ(NKX22[52],0,65,NKX22_V)

# plot_single_profile(NKX22[53],0,50)
# calculate_TZ(NKX22[53],0,50,NKX22_V)

# plot_single_profile(NKX22[54],0,50)
# calculate_TZ(NKX22[54],0,50,NKX22_V)

# plot_single_profile(NKX22[55],0,60)
# calculate_TZ(NKX22[55],0,60,NKX22_V)

# plot_single_profile(NKX22[56],0,70)
# calculate_TZ(NKX22[56],0,70,NKX22_V)

# plot_single_profile(NKX22[57],0,70)
# calculate_TZ(NKX22[57],0,70,NKX22_V)

# plot_single_profile(NKX22[58],0,70)
# calculate_TZ(NKX22[58],0,70,NKX22_V)

# plot_single_profile(NKX22[59],0,65)
# calculate_TZ(NKX22[59],0,65,NKX22_V)

# plot_single_profile(NKX22[60],0,70)
# calculate_TZ(NKX22[60],0,70,NKX22_V)

# plot_single_profile(NKX22[61],0,75)
# calculate_TZ(NKX22[61],0,75,NKX22_V)

# plot_single_profile(NKX22[62],0,60)
# calculate_TZ(NKX22[62],0,60,NKX22_V)

# plot_single_profile(NKX22[63],0,65)
# calculate_TZ(NKX22[63],0,65,NKX22_V)

# plot_single_profile(NKX22[64],0,50)
# calculate_TZ(NKX22[64],0,50,NKX22_V)

# plot_single_profile(NKX22[65],0,65)
# calculate_TZ(NKX22[65],0,65,NKX22_V)

# plot_single_profile(NKX22[66],0,70)
# calculate_TZ(NKX22[66],0,70,NKX22_V)

# plot_single_profile(NKX22[67],0,70)
# calculate_TZ(NKX22[67],0,70,NKX22_V)

# plot_single_profile(NKX22[68],0,60)
# calculate_TZ(NKX22[68],0,60,NKX22_V)

# plot_single_profile(NKX22[69],0,60)
# calculate_TZ(NKX22[69],0,60,NKX22_V)

# plot_single_profile(NKX22[70],0,50)
# calculate_TZ(NKX22[70],0,50,NKX22_V)

# plot_single_profile(NKX22[71],0,80)
# calculate_TZ(NKX22[71],0,80,NKX22_V)

# plot_single_profile(NKX22[72],0,85)
# calculate_TZ(NKX22[72],0,85,NKX22_V)

# plot_single_profile(NKX22[73],0,75)
# calculate_TZ(NKX22[73],0,75,NKX22_V)

# plot_single_profile(NKX22[74],0,55)
# calculate_TZ(NKX22[74],0,55,NKX22_V)

# plot_single_profile(NKX22[75],40,70)
# calculate_TZ(NKX22[75],40,70,NKX22_V)

# plot_single_profile(NKX22[76],10,90)
# calculate_TZ(NKX22[76],10,90,NKX22_V)

# plot_single_profile(NKX22[77],20,100)
# calculate_TZ(NKX22[77],20,100,NKX22_V)

# plot_single_profile(NKX22[78],10,90)
# calculate_TZ(NKX22[78],10,90,NKX22_V)

# plot_single_profile(NKX22[79],0,110)
# calculate_TZ(NKX22[79],0,110,NKX22_V)

# plot_single_profile(NKX22[80],0,60)
# calculate_TZ(NKX22[80],0,60,NKX22_V)

# # plot_single_profile(NKX22[81],20,50)
# # calculate_TZ(NKX22[81],20,50,NKX22_V)

# # plot_single_profile(NKX22[82],0,50)
# # calculate_TZ(NKX22[82],0,50,NKX22_V)

# # plot_single_profile(NKX22[83],0,50)
# # calculate_TZ(NKX22[83],0,50,NKX22_V)

# plot_single_profile(NKX22[84],0,40)
# calculate_TZ(NKX22[84],0,40,NKX22_V)

# # plot_single_profile(NKX22[85],20,40)
# # calculate_TZ(NKX22[85],20,40,NKX22_V)

# # plot_single_profile(NKX22[86],20,60)
# # calculate_TZ(NKX22[86],20,60,NKX22_V)

# # plot_single_profile(NKX22[87],25,38)
# # calculate_TZ(NKX22[87],25,38,NKX22_V)

# # plot_single_profile(NKX22[88],25,350)
# # calculate_TZ(NKX22[88],0,30,NKX22_V)

# # plot_single_profile(NKX22[89],0,400)
# # calculate_TZ(NKX22[89],30,40,NKX22_V)

# # plot_single_profile(NKX22[90],10,50)
# # calculate_TZ(NKX22[90],10,50,NKX22_V)

# # plot_single_profile(NKX22[91],0,60)
# # calculate_TZ(NKX22[91],0,60,NKX22_V)

# # plot_single_profile(NKX22[92],10,50)
# # calculate_TZ(NKX22[92],10,50,NKX22_V)

# # plot_single_profile(NKX22[93],10,55)
# # calculate_TZ(NKX22[93],5,50,NKX22_V)

# plot_single_profile(NKX22[94],0,50)
# calculate_TZ(NKX22[94],0,50,NKX22_V)

# # plot_single_profile(NKX22[95],20,300)
# # calculate_TZ(NKX22[95],10,20,NKX22_V)

# # plot_single_profile(NKX22[96],10,60)
# # calculate_TZ(NKX22[96],10,60,NKX22_V)

# # plot_single_profile(NKX22[97],10,60)
# # calculate_TZ(NKX22[97],10,60,NKX22_V)

# # plot_single_profile(NKX22[98],10,500)
# # calculate_TZ(NKX22[98],10,50,NKX22_V)

# # plot_single_profile(NKX22[99],0,60)
# # calculate_TZ(NKX22[99],0,60,NKX22_V)

# # plot_single_profile(NKX22[100],0,70)
# # calculate_TZ(NKX22[100],0,70,NKX22_V)

# # plot_single_profile(NKX22[101],25,150)
# # calculate_TZ(NKX22[101],25,150,NKX22_V)

# # plot_single_profile(NKX22[102],25,500)
# # calculate_TZ(NKX22[102],25,50,NKX22_V)

# # plot_single_profile(NKX22[103],0,500)
# # calculate_TZ(NKX22[103],0,80,NKX22_V)

# # NKX22_D

# plot_single_profile(NKX22[1],20,100)
# calculate_TZ(NKX22[1],20,100,NKX22_D)

# plot_single_profile(NKX22[2],40,80)
# calculate_TZ(NKX22[2],40,80,NKX22_D)

# plot_single_profile(NKX22[3],55,150)
# calculate_TZ(NKX22[3],55,150,NKX22_D)

# plot_single_profile(NKX22[4],50,100)
# calculate_TZ(NKX22[4],50,100,NKX22_D)

# plot_single_profile(NKX22[5],40,80)
# calculate_TZ(NKX22[5],40,80,NKX22_D)

# plot_single_profile(NKX22[6],40,100)
# calculate_TZ(NKX22[6],40,100,NKX22_D)

# plot_single_profile(NKX22[7],60,120)
# calculate_TZ(NKX22[7],60,120,NKX22_D)

# plot_single_profile(NKX22[8],40,180)
# calculate_TZ(NKX22[8],40,180,NKX22_D)

# plot_single_profile(NKX22[9],40,100)
# calculate_TZ(NKX22[9],40,100,NKX22_D)

# plot_single_profile(NKX22[10],50,100)
# calculate_TZ(NKX22[10],50,100,NKX22_D)

# plot_single_profile(NKX22[11],50,100)
# calculate_TZ(NKX22[11],50,100,NKX22_D)

# plot_single_profile(NKX22[12],70,120)
# calculate_TZ(NKX22[12],70,120,NKX22_D)

# plot_single_profile(NKX22[13],40,100)
# calculate_TZ(NKX22[13],40,100,NKX22_D)

# plot_single_profile(NKX22[14],50,100)
# calculate_TZ(NKX22[14],50,100,NKX22_D)

# plot_single_profile(NKX22[15],40,100)
# calculate_TZ(NKX22[15],40,100,NKX22_D)

# plot_single_profile(NKX22[16],50,100)
# calculate_TZ(NKX22[16],50,100,NKX22_D)

# plot_single_profile(NKX22[17],40,200)
# calculate_TZ(NKX22[17],40,200,NKX22_D)

# plot_single_profile(NKX22[18],50,200)
# calculate_TZ(NKX22[18],50,200,NKX22_D)

# plot_single_profile(NKX22[19],50,100)
# calculate_TZ(NKX22[19],50,100,NKX22_D)

# plot_single_profile(NKX22[20],40,100)
# calculate_TZ(NKX22[20],40,100,NKX22_D)

# plot_single_profile(NKX22[21],50,100)
# calculate_TZ(NKX22[21],50,100,NKX22_D)

# plot_single_profile(NKX22[22],50,100)
# calculate_TZ(NKX22[22],50,100,NKX22_D)

# plot_single_profile(NKX22[23],60,100)
# calculate_TZ(NKX22[23],60,100,NKX22_D)

# plot_single_profile(NKX22[24],50,200)
# calculate_TZ(NKX22[24],50,200,NKX22_D)

# plot_single_profile(NKX22[25],30,200)
# calculate_TZ(NKX22[25],30,200,NKX22_D)

# plot_single_profile(NKX22[26],40,100)
# calculate_TZ(NKX22[26],40,100,NKX22_D)

# plot_single_profile(NKX22[27],30,100)
# calculate_TZ(NKX22[27],30,100,NKX22_D)

# plot_single_profile(NKX22[28],30,100)
# calculate_TZ(NKX22[28],30,100,NKX22_D)

# plot_single_profile(NKX22[29],50,100)
# calculate_TZ(NKX22[29],50,100,NKX22_D)

# plot_single_profile(NKX22[30],50,100)
# calculate_TZ(NKX22[30],50,100,NKX22_D)

# plot_single_profile(NKX22[31],30,100)
# calculate_TZ(NKX22[31],30,100,NKX22_D)

# plot_single_profile(NKX22[32],40,200)
# calculate_TZ(NKX22[32],40,200,NKX22_D)

# plot_single_profile(NKX22[33],30,100)
# calculate_TZ(NKX22[33],30,100,NKX22_D)

# plot_single_profile(NKX22[34],30,100)
# calculate_TZ(NKX22[34],30,100,NKX22_D)

# plot_single_profile(NKX22[35],40,200)
# calculate_TZ(NKX22[35],40,200,NKX22_D)

# plot_single_profile(NKX22[36],30,100)
# calculate_TZ(NKX22[36],30,100,NKX22_D)

# plot_single_profile(NKX22[37],30,100)
# calculate_TZ(NKX22[37],30,100,NKX22_D)

# plot_single_profile(NKX22[38],40,200)
# calculate_TZ(NKX22[38],40,200,NKX22_D)

# plot_single_profile(NKX22[39],30,100)
# calculate_TZ(NKX22[39],30,100,NKX22_D)

# plot_single_profile(NKX22[40],30,100)
# calculate_TZ(NKX22[40],30,100,NKX22_D)

# plot_single_profile(NKX22[41],40,100)
# calculate_TZ(NKX22[41],40,100,NKX22_D)

# plot_single_profile(NKX22[42],30,100)
# calculate_TZ(NKX22[42],30,100,NKX22_D)

# plot_single_profile(NKX22[43],30,200)
# calculate_TZ(NKX22[43],30,200,NKX22_D)

# plot_single_profile(NKX22[44],30,100)
# calculate_TZ(NKX22[44],30,100,NKX22_D)

# plot_single_profile(NKX22[45],50,100)
# calculate_TZ(NKX22[45],50,100,NKX22_D)

# plot_single_profile(NKX22[46],30,100)
# calculate_TZ(NKX22[46],30,100,NKX22_D)

# plot_single_profile(NKX22[47],30,100)
# calculate_TZ(NKX22[47],30,100,NKX22_D)

# plot_single_profile(NKX22[48],30,100)
# calculate_TZ(NKX22[48],30,100,NKX22_D)

# plot_single_profile(NKX22[49],40,200)
# calculate_TZ(NKX22[49],40,200,NKX22_D)

# plot_single_profile(NKX22[50],40,200)
# calculate_TZ(NKX22[50],40,200,NKX22_D)

# plot_single_profile(NKX22[51],40,100)
# calculate_TZ(NKX22[51],40,100,NKX22_D)

# plot_single_profile(NKX22[52],50,100)
# calculate_TZ(NKX22[52],50,100,NKX22_D)

# plot_single_profile(NKX22[53],40,100) 
# calculate_TZ(NKX22[53],40,100,NKX22_D)

# plot_single_profile(NKX22[54],30,100)
# calculate_TZ(NKX22[54],30,100,NKX22_D)

# plot_single_profile(NKX22[55],40,150)
# calculate_TZ(NKX22[55],40,150,NKX22_D)

# plot_single_profile(NKX22[56],40,100)
# calculate_TZ(NKX22[56],40,100,NKX22_D)

# plot_single_profile(NKX22[57],40,100)
# calculate_TZ(NKX22[57],40,100,NKX22_D)

# plot_single_profile(NKX22[58],30,100)
# calculate_TZ(NKX22[58],30,100,NKX22_D)

# plot_single_profile(NKX22[59],30,100)
# calculate_TZ(NKX22[59],30,100,NKX22_D)

# plot_single_profile(NKX22[60],30,100)
# calculate_TZ(NKX22[60],30,100,NKX22_D)

# plot_single_profile(NKX22[61],50,100)
# calculate_TZ(NKX22[61],50,100,NKX22_D)

# plot_single_profile(NKX22[62],30,100)
# calculate_TZ(NKX22[62],30,100,NKX22_D)

# plot_single_profile(NKX22[63],40,100)
# calculate_TZ(NKX22[63],40,100,NKX22_D)

# plot_single_profile(NKX22[64],30,100)
# calculate_TZ(NKX22[64],30,100,NKX22_D)

# plot_single_profile(NKX22[65],50,100)
# calculate_TZ(NKX22[65],50,100,NKX22_D)

# plot_single_profile(NKX22[66],30,100)
# calculate_TZ(NKX22[66],30,100,NKX22_D)

# plot_single_profile(NKX22[67],50,150)
# calculate_TZ(NKX22[67],50,150,NKX22_D)

# plot_single_profile(NKX22[68],30,100)
# calculate_TZ(NKX22[68],30,100,NKX22_D)

# plot_single_profile(NKX22[69],30,100)
# calculate_TZ(NKX22[69],30,100,NKX22_D)

# plot_single_profile(NKX22[70],30,100)
# calculate_TZ(NKX22[70],30,100,NKX22_D)

# plot_single_profile(NKX22[71],50,100)
# calculate_TZ(NKX22[71],50,100,NKX22_D)

# plot_single_profile(NKX22[72],50,100)
# calculate_TZ(NKX22[72],50,100,NKX22_D)

# plot_single_profile(NKX22[73],50,200)
# calculate_TZ(NKX22[73],50,200,NKX22_D)

# plot_single_profile(NKX22[74],50,150)
# calculate_TZ(NKX22[74],50,150,NKX22_D)

# plot_single_profile(NKX22[75],70,150)
# calculate_TZ(NKX22[75],70,150,NKX22_D)

# plot_single_profile(NKX22[76],70,150)
# calculate_TZ(NKX22[76],70,150,NKX22_D)

# plot_single_profile(NKX22[77],70,150)
# calculate_TZ(NKX22[77],70,150,NKX22_D)

# plot_single_profile(NKX22[78],70,150) 
# calculate_TZ(NKX22[78],70,150,NKX22_D)

# plot_single_profile(NKX22[79],90,150) 
# calculate_TZ(NKX22[79],90,150,NKX22_D)

# # plot_single_profile(NKX22[80],40,100)
# # calculate_TZ(NKX22[80],40,100,NKX22_D)

# # plot_single_profile(NKX22[81],00,200)
# # calculate_TZ(NKX22[81],50,70,NKX22_D)

# # plot_single_profile(NKX22[82],0,300)
# # calculate_TZ(NKX22[82],30,100,NKX22_D)

# # plot_single_profile(NKX22[83],30,100)
# # calculate_TZ(NKX22[83],30,100,NKX22_D)

# # plot_single_profile(NKX22[84],0,350)
# # calculate_TZ(NKX22[84],0,350,NKX22_D)

# # plot_single_profile(NKX22[85],0,300)
# # calculate_TZ(NKX22[85],0,300,NKX22_D)

# # plot_single_profile(NKX22[86],0,350)
# # calculate_TZ(NKX22[86],0,350,NKX22_D)

# # plot_single_profile(NKX22[87],0,350)
# # calculate_TZ(NKX22[87],0,350,NKX22_D)

# # plot_single_profile(NKX22[88],0,350)
# # calculate_TZ(NKX22[88],0,350,NKX22_D)

# # plot_single_profile(NKX22[89],0,420)
# # calculate_TZ(NKX22[89],0,420,NKX22_D)

# # plot_single_profile(NKX22[90],0,350)
# # calculate_TZ(NKX22[90],0,350,NKX22_D)

# # plot_single_profile(NKX22[91],0,350)
# # calculate_TZ(NKX22[91],0,350,NKX22_D)

# # plot_single_profile(NKX22[92],0,350)
# # calculate_TZ(NKX22[92],0,350,NKX22_D)

# # plot_single_profile(NKX22[93],0,350)
# # calculate_TZ(NKX22[93],0,350,NKX22_D)

# # plot_single_profile(NKX22[94],50,400)
# # calculate_TZ(NKX22[94],50,400,NKX22_D)

# # plot_single_profile(NKX22[95],0,500)
# # calculate_TZ(NKX22[95],0,500,NKX22_D)

# # plot_single_profile(NKX22[96],100,200)
# # calculate_TZ(NKX22[96],100,20,NKX22_D)

# # plot_single_profile(NKX22[97],50,90)
# # calculate_TZ(NKX22[97],50,90,NKX22_D)

# # plot_single_profile(NKX22[98],0,300)
# # calculate_TZ(NKX22[98],0,300,NKX22_D)

# # plot_single_profile(NKX22[99],0,300)
# # calculate_TZ(NKX22[99],0,300,NKX22_D)

# # plot_single_profile(NKX22[100],50,400)
# # calculate_TZ(NKX22[100],50,400,NKX22_D)

# # plot_single_profile(NKX22[101],50,400)
# # calculate_TZ(NKX22[101],50,400,NKX22_D)

# # plot_single_profile(NKX22[102],0,500)
# # calculate_TZ(NKX22[102],0,500,NKX22_D)

# # plot_single_profile(NKX22[103],0,350)
# # calculate_TZ(NKX22[103],50,350,NKX22_D)



# # NKX61_V

# plot_single_profile(NKX61[1],0,10)
# calculate_TZ(NKX61[1],0,10,NKX61_V)

# plot_single_profile(NKX61[2],0,15)
# calculate_TZ(NKX61[2],0,15,NKX61_V)

# plot_single_profile(NKX61[3],0,10)
# calculate_TZ(NKX61[3],0,10,NKX61_V)

# plot_single_profile(NKX61[4],0,15)
# calculate_TZ(NKX61[4],0,15,NKX61_V)

# plot_single_profile(NKX61[5],0,15)
# calculate_TZ(NKX61[5],0,15,NKX61_V)

# # plot_single_profile(NKX61[6],0,45)
# # calculate_TZ(NKX61[6],0,70,NKX61_V)

# plot_single_profile(NKX61[7],0,10)
# calculate_TZ(NKX61[7],0,10,NKX61_V)

# plot_single_profile(NKX61[8],0,20)
# calculate_TZ(NKX61[8],0,20,NKX61_V)

# plot_single_profile(NKX61[9],0,10)
# calculate_TZ(NKX61[9],0,10,NKX61_V)

# plot_single_profile(NKX61[10],0,20)
# calculate_TZ(NKX61[10],0,20,NKX61_V)

# plot_single_profile(NKX61[11],0,10)
# calculate_TZ(NKX61[11],0,10,NKX61_V)

# plot_single_profile(NKX61[12],0,20)
# calculate_TZ(NKX61[12],0,20,NKX61_V)

# plot_single_profile(NKX61[13],0,10)
# calculate_TZ(NKX61[13],0,10,NKX61_V)

# plot_single_profile(NKX61[14],0,15)
# calculate_TZ(NKX61[14],0,15,NKX61_V)

# plot_single_profile(NKX61[15],0,10)
# calculate_TZ(NKX61[15],0,10,NKX61_V)

# plot_single_profile(NKX61[16],0,15)
# calculate_TZ(NKX61[16],0,15,NKX61_V)

# plot_single_profile(NKX61[17],0,15)
# calculate_TZ(NKX61[17],0,15,NKX61_V)

# plot_single_profile(NKX61[18],0,25)
# calculate_TZ(NKX61[18],0,25,NKX61_V)

# plot_single_profile(NKX61[19],0,12)
# calculate_TZ(NKX61[19],0,12,NKX61_V)

# plot_single_profile(NKX61[20],0,10)
# calculate_TZ(NKX61[20],0,10,NKX61_V)

# plot_single_profile(NKX61[21],0,15)
# calculate_TZ(NKX61[21],0,15,NKX61_V)

# plot_single_profile(NKX61[22],0,15)
# calculate_TZ(NKX61[22],0,15,NKX61_V)

# plot_single_profile(NKX61[23],0,15)
# calculate_TZ(NKX61[23],0,15,NKX61_V)

# plot_single_profile(NKX61[24],0,10)
# calculate_TZ(NKX61[24],0,10,NKX61_V)

# plot_single_profile(NKX61[25],0,25)
# calculate_TZ(NKX61[25],0,25,NKX61_V)

# plot_single_profile(NKX61[26],0,10)
# calculate_TZ(NKX61[26],0,10,NKX61_V)

# plot_single_profile(NKX61[27],0,10)
# calculate_TZ(NKX61[27],0,10,NKX61_V)

# plot_single_profile(NKX61[28],0,20)
# calculate_TZ(NKX61[28],0,20,NKX61_V)

# plot_single_profile(NKX61[29],0,10)
# calculate_TZ(NKX61[29],0,10,NKX61_V)

# plot_single_profile(NKX61[30],0,20)
# calculate_TZ(NKX61[30],0,20,NKX61_V)

# plot_single_profile(NKX61[31],0,10)
# calculate_TZ(NKX61[31],0,10,NKX61_V)

# plot_single_profile(NKX61[32],0,20)
# calculate_TZ(NKX61[32],0,20,NKX61_V)

# plot_single_profile(NKX61[33],0,15)
# calculate_TZ(NKX61[33],0,15,NKX61_V)

# plot_single_profile(NKX61[34],0,15)
# calculate_TZ(NKX61[34],0,15,NKX61_V)

# plot_single_profile(NKX61[35],0,15)
# calculate_TZ(NKX61[35],0,15,NKX61_V)

# plot_single_profile(NKX61[36],0,10)
# calculate_TZ(NKX61[36],0,10,NKX61_V)

# plot_single_profile(NKX61[37],0,15)
# calculate_TZ(NKX61[37],0,15,NKX61_V)

# plot_single_profile(NKX61[38],0,10)
# calculate_TZ(NKX61[38],0,10,NKX61_V)

# plot_single_profile(NKX61[39],0,10)
# calculate_TZ(NKX61[39],0,10,NKX61_V)

# plot_single_profile(NKX61[40],0,10)
# calculate_TZ(NKX61[40],0,10,NKX61_V)

# plot_single_profile(NKX61[41],0,10)
# calculate_TZ(NKX61[41],0,10,NKX61_V)

# plot_single_profile(NKX61[42],0,15)
# calculate_TZ(NKX61[42],0,15,NKX61_V)

# plot_single_profile(NKX61[43],0,5)
# calculate_TZ(NKX61[43],0,5,NKX61_V)

# plot_single_profile(NKX61[44],0,8)
# calculate_TZ(NKX61[44],0,8,NKX61_V)

# plot_single_profile(NKX61[45],0,10)
# calculate_TZ(NKX61[45],0,10,NKX61_V)

# plot_single_profile(NKX61[46],0,8)
# calculate_TZ(NKX61[46],0,8,NKX61_V)

# plot_single_profile(NKX61[47],0,8)
# calculate_TZ(NKX61[47],0,8,NKX61_V)

# plot_single_profile(NKX61[48],0,10)
# calculate_TZ(NKX61[48],0,10,NKX61_V)

# plot_single_profile(NKX61[49],0,15)
# calculate_TZ(NKX61[49],0,15,NKX61_V)

# plot_single_profile(NKX61[50],0,10)
# calculate_TZ(NKX61[50],0,10,NKX61_V)

# plot_single_profile(NKX61[51],0,15)
# calculate_TZ(NKX61[51],0,15,NKX61_V)

# plot_single_profile(NKX61[52],0,15)
# calculate_TZ(NKX61[52],0,15,NKX61_V)

# plot_single_profile(NKX61[53],0,15)
# calculate_TZ(NKX61[53],0,15,NKX61_V)

# plot_single_profile(NKX61[54],0,10)
# calculate_TZ(NKX61[54],0,10,NKX61_V)

# plot_single_profile(NKX61[55],0,10)
# calculate_TZ(NKX61[55],0,10,NKX61_V)

# plot_single_profile(NKX61[56],0,15)
# calculate_TZ(NKX61[56],0,15,NKX61_V)

# plot_single_profile(NKX61[57],0,8)
# calculate_TZ(NKX61[57],0,8,NKX61_V)

# plot_single_profile(NKX61[58],0,15)
# calculate_TZ(NKX61[58],0,15,NKX61_V)

# plot_single_profile(NKX61[59],0,5)
# calculate_TZ(NKX61[59],0,5,NKX61_V)

# plot_single_profile(NKX61[60],0,10)
# calculate_TZ(NKX61[60],0,10,NKX61_V)

# # plot_single_profile(NKX61[61],0,4)
# # calculate_TZ(NKX61[61],0,4,NKX61_V)

# plot_single_profile(NKX61[62],0,15)
# calculate_TZ(NKX61[62],0,15,NKX61_V)

# plot_single_profile(NKX61[63],0,10)
# calculate_TZ(NKX61[63],0,10,NKX61_V)

# # plot_single_profile(NKX61[64],0,40)
# # calculate_TZ(NKX61[64],0,50,NKX61_V)

# # plot_single_profile(NKX61[65],0,7)
# # calculate_TZ(NKX61[65],0,10,NKX61_V)

# # plot_single_profile(NKX61[66],0,20)
# # calculate_TZ(NKX61[66],0,15,NKX61_V)

# plot_single_profile(NKX61[67],0,30)
# calculate_TZ(NKX61[67],0,30,NKX61_V)

# plot_single_profile(NKX61[68],0,30)
# calculate_TZ(NKX61[68],0,30,NKX61_V)

# plot_single_profile(NKX61[69],0,15)
# calculate_TZ(NKX61[69],0,15,NKX61_V)

# plot_single_profile(NKX61[70],0,15)
# calculate_TZ(NKX61[70],0,15,NKX61_V)

# plot_single_profile(NKX61[71],0,20)
# calculate_TZ(NKX61[71],0,20,NKX61_V)

# plot_single_profile(NKX61[72],0,10)
# calculate_TZ(NKX61[72],0,10,NKX61_V)

# plot_single_profile(NKX61[73],0,5)
# calculate_TZ(NKX61[73],0,5,NKX61_V)

# plot_single_profile(NKX61[74],0,5)
# calculate_TZ(NKX61[74],0,5,NKX61_V)

# plot_single_profile(NKX61[75],0,10)
# calculate_TZ(NKX61[75],0,10,NKX61_V)

# plot_single_profile(NKX61[76],0,4)
# calculate_TZ(NKX61[76],0,4,NKX61_V)

# # plot_single_profile(NKX61[77],0,4)
# # calculate_TZ(NKX61[77],0,4,NKX61_V)

# plot_single_profile(NKX61[78],0,15)
# calculate_TZ(NKX61[78],0,15,NKX61_V)

# plot_single_profile(NKX61[79],0,10)
# calculate_TZ(NKX61[79],0,10,NKX61_V)

# plot_single_profile(NKX61[80],0,10)
# calculate_TZ(NKX61[80],0,10,NKX61_V)

# plot_single_profile(NKX61[81],0,5)
# calculate_TZ(NKX61[81],0,5,NKX61_V)

# plot_single_profile(NKX61[82],0,5)
# calculate_TZ(NKX61[82],0,5,NKX61_V)

# plot_single_profile(NKX61[83],0,10)
# calculate_TZ(NKX61[83],0,10,NKX61_V)

# plot_single_profile(NKX61[84],0,15)
# calculate_TZ(NKX61[84],0,15,NKX61_V)

# # plot_single_profile(NKX61[85],0,25)
# # calculate_TZ(NKX61[85],0,15,NKX61_V)

# plot_single_profile(NKX61[86],0,15)
# calculate_TZ(NKX61[86],0,15,NKX61_V)

# plot_single_profile(NKX61[87],0,15)
# calculate_TZ(NKX61[87],0,15,NKX61_V)

# plot_single_profile(NKX61[88],0,15)
# calculate_TZ(NKX61[88],0,15,NKX61_V)

# plot_single_profile(NKX61[89],0,80)
# calculate_TZ(NKX61[89],0,80,NKX61_V)

# plot_single_profile(NKX61[90],0,120)
# calculate_TZ(NKX61[90],0,120,NKX61_V)

# # plot_single_profile(NKX61[91],0,70)
# # calculate_TZ(NKX61[91],0,20,NKX61_V)

# plot_single_profile(NKX61[92],0,150)
# calculate_TZ(NKX61[92],0,150,NKX61_V)

# # plot_single_profile(NKX61[93],0,100)
# # calculate_TZ(NKX61[93],0,18,NKX61_V)

# plot_single_profile(NKX61[94],0,10)
# calculate_TZ(NKX61[94],0,10,NKX61_V)

# plot_single_profile(NKX61[95],0,5)
# calculate_TZ(NKX61[95],0,5,NKX61_V)

# # plot_single_profile(NKX61[96],0,15)
# # calculate_TZ(NKX61[96],0,5,NKX61_V)

# plot_single_profile(NKX61[97],0,150)
# calculate_TZ(NKX61[97],0,150,NKX61_V)

# plot_single_profile(NKX61[98],0,150)
# calculate_TZ(NKX61[98],0,150,NKX61_V)

# plot_single_profile(NKX61[99],0,100)
# calculate_TZ(NKX61[99],0,100,NKX61_V)

# plot_single_profile(NKX61[100],0,75)
# calculate_TZ(NKX61[100],0,75,NKX61_V)

# # plot_single_profile(NKX61[101],0,10)
# # calculate_TZ(NKX61[101],0,75,NKX61_V)

# # plot_single_profile(NKX61[102],0,50)
# # calculate_TZ(NKX61[102],0,100,NKX61_V)

# plot_single_profile(NKX61[103],0,50)
# calculate_TZ(NKX61[103],0,50,NKX61_V)

# plot_single_profile(NKX61[104],0,60)
# calculate_TZ(NKX61[104],0,60,NKX61_V)

# # plot_single_profile(NKX61[105],0,5)
# # calculate_TZ(NKX61[105],0,5,NKX61_V)

# # plot_single_profile(NKX61[106],0,10)
# # calculate_TZ(NKX61[106],0,20,NKX61_V)

# # plot_single_profile(NKX61[107],0,20)
# # calculate_TZ(NKX61[107],0,20,NKX61_V)

# # plot_single_profile(NKX61[108],0,5)
# # calculate_TZ(NKX61[108],0,5,NKX61_V)

# # plot_single_profile(NKX61[109],0,5)
# # calculate_TZ(NKX61[109],0,5,NKX61_V)

# # plot_single_profile(NKX61[110],0,10)
# # calculate_TZ(NKX61[110],0,10,NKX61_V)

# plot_single_profile(NKX61[111],0,100)
# calculate_TZ(NKX61[111],0,100,NKX61_V)

# plot_single_profile(NKX61[112],0,150)
# calculate_TZ(NKX61[112],0,20,NKX61_V)


# # NKX61_D

# plot_single_profile(NKX61[1],100,200)
# calculate_TZ(NKX61[1],100,200,NKX61_D)

# plot_single_profile(NKX61[2],100,200)
# calculate_TZ(NKX61[2],100,200,NKX61_D)

# plot_single_profile(NKX61[3],100,250)
# calculate_TZ(NKX61[3],100,250,NKX61_D)

# plot_single_profile(NKX61[4],100,200)
# calculate_TZ(NKX61[4],100,200,NKX61_D)

# plot_single_profile(NKX61[5],100,250)
# calculate_TZ(NKX61[5],100,250,NKX61_D)

# plot_single_profile(NKX61[6],100,200)
# calculate_TZ(NKX61[6],100,200,NKX61_D)

# plot_single_profile(NKX61[7],0,500)
# calculate_TZ(NKX61[7],0,500,NKX61_D)

# plot_single_profile(NKX61[8],100,200)
# calculate_TZ(NKX61[8],100,200,NKX61_D)

# plot_single_profile(NKX61[9],100,200)
# calculate_TZ(NKX61[9],100,200,NKX61_D)

# plot_single_profile(NKX61[10],130,200)
# calculate_TZ(NKX61[10],130,200,NKX61_D)

# plot_single_profile(NKX61[11],100,200)
# calculate_TZ(NKX61[11],100,200,NKX61_D)

# plot_single_profile(NKX61[12],0,500)
# calculate_TZ(NKX61[12],0,500,NKX61_D)

# plot_single_profile(NKX61[13],100,200)
# calculate_TZ(NKX61[13],100,200,NKX61_D)

# plot_single_profile(NKX61[14],0,500)
# calculate_TZ(NKX61[14],0,500,NKX61_D)

# plot_single_profile(NKX61[15],0,500)
# calculate_TZ(NKX61[15],0,500,NKX61_D)

# plot_single_profile(NKX61[16],0,500)
# calculate_TZ(NKX61[16],0,500,NKX61_D)

# plot_single_profile(NKX61[17],50,500)
# calculate_TZ(NKX61[17],50,500,NKX61_D)

# plot_single_profile(NKX61[18],0,500)
# calculate_TZ(NKX61[18],0,500,NKX61_D)

# plot_single_profile(NKX61[19],0,500)
# calculate_TZ(NKX61[19],0,500,NKX61_D)

# plot_single_profile(NKX61[20],50,500)
# calculate_TZ(NKX61[20],50,500,NKX61_D)

# plot_single_profile(NKX61[21],100,300)
# calculate_TZ(NKX61[21],100,300,NKX61_D)

# plot_single_profile(NKX61[22],50,500)
# calculate_TZ(NKX61[22],50,500,NKX61_D)

# plot_single_profile(NKX61[23],50,500)
# calculate_TZ(NKX61[23],50,500,NKX61_D)

# plot_single_profile(NKX61[24],0,500)
# calculate_TZ(NKX61[24],0,500,NKX61_D)

# plot_single_profile(NKX61[25],0,500)
# calculate_TZ(NKX61[25],0,500,NKX61_D)

# plot_single_profile(NKX61[26],0,500)
# calculate_TZ(NKX61[26],0,500,NKX61_D)

# plot_single_profile(NKX61[27],0,500)
# calculate_TZ(NKX61[27],0,500,NKX61_D)

# plot_single_profile(NKX61[28],0,500)
# calculate_TZ(NKX61[28],0,500,NKX61_D)

# plot_single_profile(NKX61[29],0,500)
# calculate_TZ(NKX61[29],0,500,NKX61_D)

# plot_single_profile(NKX61[30],0,500)
# calculate_TZ(NKX61[30],0,500,NKX61_D)

# plot_single_profile(NKX61[31],0,500)
# calculate_TZ(NKX61[31],0,500,NKX61_D)

# plot_single_profile(NKX61[32],0,500)
# calculate_TZ(NKX61[32],0,500,NKX61_D)

# plot_single_profile(NKX61[33],50,500)
# calculate_TZ(NKX61[33],50,500,NKX61_D)

# plot_single_profile(NKX61[34],0,500)
# calculate_TZ(NKX61[34],0,500,NKX61_D)

# plot_single_profile(NKX61[35],0,500)
# calculate_TZ(NKX61[35],0,500,NKX61_D)

# plot_single_profile(NKX61[36],0,500)
# calculate_TZ(NKX61[36],0,500,NKX61_D)

# plot_single_profile(NKX61[37],0,500)
# calculate_TZ(NKX61[37],0,500,NKX61_D)

# plot_single_profile(NKX61[38],0,500)
# calculate_TZ(NKX61[38],0,500,NKX61_D)

# plot_single_profile(NKX61[39],0,500)
# calculate_TZ(NKX61[39],0,500,NKX61_D)

# plot_single_profile(NKX61[40],0,500)
# calculate_TZ(NKX61[40],0,500,NKX61_D)

# plot_single_profile(NKX61[41],0,500)
# calculate_TZ(NKX61[41],0,500,NKX61_D)

# plot_single_profile(NKX61[42],0,500)
# calculate_TZ(NKX61[42],0,500,NKX61_D)

# plot_single_profile(NKX61[43],100,250)
# calculate_TZ(NKX61[43],100,250,NKX61_D)

# plot_single_profile(NKX61[44],50,500)
# calculate_TZ(NKX61[44],50,500,NKX61_D)

# plot_single_profile(NKX61[45],100,500)
# calculate_TZ(NKX61[45],100,500,NKX61_D)

# plot_single_profile(NKX61[46],100,500)
# calculate_TZ(NKX61[46],100,500,NKX61_D)

# plot_single_profile(NKX61[47],0,500)
# calculate_TZ(NKX61[47],0,500,NKX61_D)

# plot_single_profile(NKX61[48],0,500)
# calculate_TZ(NKX61[48],0,500,NKX61_D)

# plot_single_profile(NKX61[49],0,500)
# calculate_TZ(NKX61[49],0,500,NKX61_D)

# plot_single_profile(NKX61[50],0,500)
# calculate_TZ(NKX61[50],0,500,NKX61_D)

# plot_single_profile(NKX61[51],0,500)
# calculate_TZ(NKX61[51],0,500,NKX61_D)

# plot_single_profile(NKX61[52],0,500)
# calculate_TZ(NKX61[52],0,500,NKX61_D)

# plot_single_profile(NKX61[53],0,500)
# calculate_TZ(NKX61[53],0,500,NKX61_D)

# plot_single_profile(NKX61[54],0,500)
# calculate_TZ(NKX61[54],0,500,NKX61_D)

# plot_single_profile(NKX61[55],0,500)
# calculate_TZ(NKX61[55],0,500,NKX61_D)

# plot_single_profile(NKX61[56],0,500)
# calculate_TZ(NKX61[56],0,500,NKX61_D)

# plot_single_profile(NKX61[57],50,500)
# calculate_TZ(NKX61[57],50,500,NKX61_D)

# plot_single_profile(NKX61[58],0,500)
# calculate_TZ(NKX61[58],0,500,NKX61_D)

# plot_single_profile(NKX61[59],0,500)
# calculate_TZ(NKX61[59],0,500,NKX61_D)

# plot_single_profile(NKX61[60],0,500)
# calculate_TZ(NKX61[60],0,500,NKX61_D)

# plot_single_profile(NKX61[61],0,500)
# calculate_TZ(NKX61[61],0,500,NKX61_D)

# plot_single_profile(NKX61[62],0,500)
# calculate_TZ(NKX61[62],0,500,NKX61_D)

# plot_single_profile(NKX61[63],0,500)
# calculate_TZ(NKX61[63],0,500,NKX61_D)

# plot_single_profile(NKX61[64],50,500)
# calculate_TZ(NKX61[64],50,500,NKX61_D)

# plot_single_profile(NKX61[65],0,500)
# calculate_TZ(NKX61[65],0,500,NKX61_D)

# plot_single_profile(NKX61[66],0,500)
# calculate_TZ(NKX61[66],0,500,NKX61_D)

# plot_single_profile(NKX61[67],0,500)
# calculate_TZ(NKX61[67],0,500,NKX61_D)

# plot_single_profile(NKX61[68],0,500)
# calculate_TZ(NKX61[68],0,500,NKX61_D)

# plot_single_profile(NKX61[69],0,500)
# calculate_TZ(NKX61[69],0,500,NKX61_D)

# plot_single_profile(NKX61[70],0,500)
# calculate_TZ(NKX61[70],0,500,NKX61_D)

# plot_single_profile(NKX61[71],0,500)
# calculate_TZ(NKX61[71],0,500,NKX61_D)

# plot_single_profile(NKX61[72],0,500)
# calculate_TZ(NKX61[72],0,500,NKX61_D)

# plot_single_profile(NKX61[73],70,500)
# calculate_TZ(NKX61[73],70,500,NKX61_D)

# plot_single_profile(NKX61[74],50,500)
# calculate_TZ(NKX61[74],50,500,NKX61_D)

# plot_single_profile(NKX61[75],0,500)
# calculate_TZ(NKX61[75],0,500,NKX61_D)

# plot_single_profile(NKX61[76],0,500)
# calculate_TZ(NKX61[76],0,500,NKX61_D)

# plot_single_profile(NKX61[77],0,500)
# calculate_TZ(NKX61[77],0,500,NKX61_D)

# plot_single_profile(NKX61[78],0,500)
# calculate_TZ(NKX61[78],0,500,NKX61_D)

# plot_single_profile(NKX61[79],80,500)
# calculate_TZ(NKX61[79],80,500,NKX61_D)

# plot_single_profile(NKX61[80],50,500)
# calculate_TZ(NKX61[80],50,500,NKX61_D)

# plot_single_profile(NKX61[81],50,500)
# calculate_TZ(NKX61[81],50,500,NKX61_D)

# plot_single_profile(NKX61[82],0,500)
# calculate_TZ(NKX61[82],0,500,NKX61_D)

# plot_single_profile(NKX61[83],50,500)
# calculate_TZ(NKX61[83],50,500,NKX61_D)

# plot_single_profile(NKX61[84],0,500)
# calculate_TZ(NKX61[84],0,500,NKX61_D)

# plot_single_profile(NKX61[85],50,500)
# calculate_TZ(NKX61[85],50,500,NKX61_D)

# plot_single_profile(NKX61[86],0,500)
# calculate_TZ(NKX61[86],0,500,NKX61_D)

# plot_single_profile(NKX61[87],50,500)
# calculate_TZ(NKX61[87],50,500,NKX61_D)

# plot_single_profile(NKX61[88],0,500)
# calculate_TZ(NKX61[88],0,500,NKX61_D)

# plot_single_profile(NKX61[89],50,300)
# calculate_TZ(NKX61[89],50,300,NKX61_D)

# plot_single_profile(NKX61[90],0,500)
# calculate_TZ(NKX61[90],0,500,NKX61_D)

# plot_single_profile(NKX61[91],0,500)
# calculate_TZ(NKX61[91],0,500,NKX61_D)

# plot_single_profile(NKX61[92],50,500)
# calculate_TZ(NKX61[92],50,500,NKX61_D)

# plot_single_profile(NKX61[93],50,300)
# calculate_TZ(NKX61[93],50,300,NKX61_D)

# plot_single_profile(NKX61[94],0,500)
# calculate_TZ(NKX61[94],0,500,NKX61_D)

# plot_single_profile(NKX61[95],50,200)
# calculate_TZ(NKX61[95],50,200,NKX61_D)

# plot_single_profile(NKX61[96],0,500)
# calculate_TZ(NKX61[96],0,500,NKX61_D)

# plot_single_profile(NKX61[97],50,300)
# calculate_TZ(NKX61[97],50,300,NKX61_D)

# plot_single_profile(NKX61[98],50,500)
# calculate_TZ(NKX61[98],50,500,NKX61_D)

# plot_single_profile(NKX61[99],50,300)
# calculate_TZ(NKX61[99],50,300,NKX61_D)

# plot_single_profile(NKX61[100],50,300)
# calculate_TZ(NKX61[100],50,300,NKX61_D)

# plot_single_profile(NKX61[101],50,300)
# calculate_TZ(NKX61[101],50,300,NKX61_D)

# plot_single_profile(NKX61[102],50,300)
# calculate_TZ(NKX61[102],50,300,NKX61_D)

# plot_single_profile(NKX61[103],60,300)
# calculate_TZ(NKX61[103],60,300,NKX61_D)

# plot_single_profile(NKX61[104],60,300)
# calculate_TZ(NKX61[104],60,300,NKX61_D)

# plot_single_profile(NKX61[105],60,300)
# calculate_TZ(NKX61[105],60,300,NKX61_D)

# plot_single_profile(NKX61[106],60,300)
# calculate_TZ(NKX61[106],60,300,NKX61_D)

# plot_single_profile(NKX61[107],60,300)
# calculate_TZ(NKX61[107],60,300,NKX61_D)

# plot_single_profile(NKX61[108],60,300)
# calculate_TZ(NKX61[108],60,300,NKX61_D)

# plot_single_profile(NKX61[109],60,300)
# calculate_TZ(NKX61[109],60,300,NKX61_D)

# plot_single_profile(NKX61[110],60,300)
# calculate_TZ(NKX61[110],60,300,NKX61_D)

# plot_single_profile(NKX61[111],60,300)
# calculate_TZ(NKX61[111],60,300,NKX61_D)

# plot_single_profile(NKX61[112],60,300)
# calculate_TZ(NKX61[112],60,300,NKX61_D)


# #  OLIG2_V


# plot_single_profile(OLIG2[1],0,150)
# calculate_TZ(OLIG2[1],0,150,OLIG2_V)

# plot_single_profile(OLIG2[2],0,200)
# calculate_TZ(OLIG2[2],0,200,OLIG2_V)

# plot_single_profile(OLIG2[3],0,100)
# calculate_TZ(OLIG2[3],0,100,OLIG2_V)

# plot_single_profile(OLIG2[4],0,100)
# calculate_TZ(OLIG2[4],0,100,OLIG2_V)

# plot_single_profile(OLIG2[5],0,110)
# calculate_TZ(OLIG2[5],0,110,OLIG2_V)

# plot_single_profile(OLIG2[6],40,120)
# calculate_TZ(OLIG2[6],40,120,OLIG2_V)

# plot_single_profile(OLIG2[7],50,100)
# calculate_TZ(OLIG2[7],0,100,OLIG2_V)

# plot_single_profile(OLIG2[8],0,120)
# calculate_TZ(OLIG2[8],0,120,OLIG2_V)

# plot_single_profile(OLIG2[9],0,120)
# calculate_TZ(OLIG2[9],0,120,OLIG2_V)

# plot_single_profile(OLIG2[10],0,100)
# calculate_TZ(OLIG2[10],0,100,OLIG2_V)

# plot_single_profile(OLIG2[11],0,120)
# calculate_TZ(OLIG2[11],0,120,OLIG2_V)

# plot_single_profile(OLIG2[12],50,110)
# calculate_TZ(OLIG2[12],50,110,OLIG2_V)

# plot_single_profile(OLIG2[13],0,120)
# calculate_TZ(OLIG2[13],0,120,OLIG2_V)

# plot_single_profile(OLIG2[14],50,120)
# calculate_TZ(OLIG2[14],50,120,OLIG2_V)

# plot_single_profile(OLIG2[15],20,120)
# calculate_TZ(OLIG2[15],20,120,OLIG2_V)

# plot_single_profile(OLIG2[16],30,90)
# calculate_TZ(OLIG2[16],30,90,OLIG2_V)

# plot_single_profile(OLIG2[17],20,100)
# calculate_TZ(OLIG2[17],20,100,OLIG2_V)

# plot_single_profile(OLIG2[18],0,120)
# calculate_TZ(OLIG2[18],0,120,OLIG2_V)

# plot_single_profile(OLIG2[19],40,120)
# calculate_TZ(OLIG2[19],40,120,OLIG2_V)

# plot_single_profile(OLIG2[20],20,120)
# calculate_TZ(OLIG2[20],20,120,OLIG2_V)

# plot_single_profile(OLIG2[21],50,100)
# calculate_TZ(OLIG2[21],50,100,OLIG2_V)

# plot_single_profile(OLIG2[22],40,120)
# calculate_TZ(OLIG2[22],40,120,OLIG2_V)

# # plot_single_profile(OLIG2[23],60,90)
# # calculate_TZ(OLIG2[23],60,90,OLIG2_V)

# plot_single_profile(OLIG2[24],40,110)
# calculate_TZ(OLIG2[24],40,110,OLIG2_V)

# plot_single_profile(OLIG2[25],30,100)
# calculate_TZ(OLIG2[25],30,100,OLIG2_V)

# plot_single_profile(OLIG2[26],30,80)
# calculate_TZ(OLIG2[26],30,80,OLIG2_V)

# plot_single_profile(OLIG2[27],30,70)
# calculate_TZ(OLIG2[27],30,70,OLIG2_V)

# plot_single_profile(OLIG2[28],30,70)
# calculate_TZ(OLIG2[28],30,70,OLIG2_V)

# plot_single_profile(OLIG2[29],40,90)
# calculate_TZ(OLIG2[29],40,90,OLIG2_V)

# plot_single_profile(OLIG2[30],40,90)
# calculate_TZ(OLIG2[30],40,90,OLIG2_V)

# plot_single_profile(OLIG2[31],30,90)
# calculate_TZ(OLIG2[31],30,90,OLIG2_V)

# plot_single_profile(OLIG2[32],30,90)
# calculate_TZ(OLIG2[32],30,90,OLIG2_V)

# plot_single_profile(OLIG2[33],30,90)
# calculate_TZ(OLIG2[33],30,90,OLIG2_V)

# plot_single_profile(OLIG2[34],30,90)
# calculate_TZ(OLIG2[34],30,90,OLIG2_V)

# plot_single_profile(OLIG2[35],30,90)
# calculate_TZ(OLIG2[35],30,90,OLIG2_V)

# plot_single_profile(OLIG2[36],30,90)
# calculate_TZ(OLIG2[36],30,90,OLIG2_V)

# plot_single_profile(OLIG2[37],30,90)
# calculate_TZ(OLIG2[37],30,90,OLIG2_V)

# plot_single_profile(OLIG2[38],30,90)
# calculate_TZ(OLIG2[38],30,90,OLIG2_V)

# plot_single_profile(OLIG2[39],30,90)
# calculate_TZ(OLIG2[39],30,90,OLIG2_V)

# plot_single_profile(OLIG2[40],30,90)
# calculate_TZ(OLIG2[40],30,90,OLIG2_V)

# plot_single_profile(OLIG2[41],40,90)
# calculate_TZ(OLIG2[41],40,90,OLIG2_V)

# plot_single_profile(OLIG2[42],40,90)
# calculate_TZ(OLIG2[42],40,90,OLIG2_V)

# plot_single_profile(OLIG2[43],30,100)
# calculate_TZ(OLIG2[43],30,100,OLIG2_V)

# plot_single_profile(OLIG2[44],30,100)
# calculate_TZ(OLIG2[44],30,100,OLIG2_V)

# plot_single_profile(OLIG2[45],30,100)
# calculate_TZ(OLIG2[45],30,100,OLIG2_V)

# plot_single_profile(OLIG2[46],30,100)
# calculate_TZ(OLIG2[46],30,100,OLIG2_V)

# plot_single_profile(OLIG2[47],30,100)
# calculate_TZ(OLIG2[47],30,100,OLIG2_V)

# plot_single_profile(OLIG2[48],30,100)
# calculate_TZ(OLIG2[48],30,100,OLIG2_V)

# plot_single_profile(OLIG2[49],30,100)
# calculate_TZ(OLIG2[49],30,100,OLIG2_V)

# plot_single_profile(OLIG2[50],30,100)
# calculate_TZ(OLIG2[50],30,100,OLIG2_V)

# plot_single_profile(OLIG2[51],30,100)
# calculate_TZ(OLIG2[51],30,100,OLIG2_V)

# plot_single_profile(OLIG2[52],30,100)
# calculate_TZ(OLIG2[52],30,100,OLIG2_V)

# plot_single_profile(OLIG2[53],30,100)
# calculate_TZ(OLIG2[53],30,100,OLIG2_V)

# plot_single_profile(OLIG2[54],30,110)
# calculate_TZ(OLIG2[54],30,110,OLIG2_V)

# plot_single_profile(OLIG2[55],30,100)
# calculate_TZ(OLIG2[55],30,100,OLIG2_V)

# plot_single_profile(OLIG2[56],30,100)
# calculate_TZ(OLIG2[56],30,100,OLIG2_V)

# plot_single_profile(OLIG2[57],30,100)
# calculate_TZ(OLIG2[57],30,100,OLIG2_V)

# plot_single_profile(OLIG2[58],30,100)
# calculate_TZ(OLIG2[58],30,100,OLIG2_V)

# plot_single_profile(OLIG2[59],20,100)
# calculate_TZ(OLIG2[59],20,100,OLIG2_V)

# plot_single_profile(OLIG2[60],20,120)
# calculate_TZ(OLIG2[60],20,120,OLIG2_V)

# plot_single_profile(OLIG2[61],20,120)
# calculate_TZ(OLIG2[61],20,120,OLIG2_V)

# plot_single_profile(OLIG2[62],20,120)
# calculate_TZ(OLIG2[62],20,120,OLIG2_V)

# plot_single_profile(OLIG2[63],20,120)
# calculate_TZ(OLIG2[63],20,120,OLIG2_V)

# plot_single_profile(OLIG2[64],20,100)
# calculate_TZ(OLIG2[64],20,100,OLIG2_V)

# plot_single_profile(OLIG2[65],20,120)
# calculate_TZ(OLIG2[65],20,120,OLIG2_V)

# plot_single_profile(OLIG2[66],20,120)
# calculate_TZ(OLIG2[66],20,120,OLIG2_V)

# plot_single_profile(OLIG2[67],30,110)
# calculate_TZ(OLIG2[67],40,110,OLIG2_V)

# plot_single_profile(OLIG2[68],20,120)
# calculate_TZ(OLIG2[68],20,120,OLIG2_V)

# plot_single_profile(OLIG2[69],30,100)
# calculate_TZ(OLIG2[69],30,100,OLIG2_V)

# plot_single_profile(OLIG2[70],20,100)
# calculate_TZ(OLIG2[70],20,100,OLIG2_V)

# plot_single_profile(OLIG2[71],20,100)
# calculate_TZ(OLIG2[71],20,100,OLIG2_V)

# plot_single_profile(OLIG2[72],20,100)
# calculate_TZ(OLIG2[72],20,100,OLIG2_V)

# plot_single_profile(OLIG2[73],20,100)
# calculate_TZ(OLIG2[73],20,100,OLIG2_V)

# # plot_single_profile(OLIG2[74],30,60)
# # calculate_TZ(OLIG2[74],30,60,OLIG2_V)

# plot_single_profile(OLIG2[75],10,70)
# calculate_TZ(OLIG2[75],10,70,OLIG2_V)

# plot_single_profile(OLIG2[76],0,60)
# calculate_TZ(OLIG2[76],0,60,OLIG2_V)

# # plot_single_profile(OLIG2[77],0,100)
# # calculate_TZ(OLIG2[77],0,60,OLIG2_V)

# plot_single_profile(OLIG2[78],0,60)
# calculate_TZ(OLIG2[78],0,60,OLIG2_V)

# plot_single_profile(OLIG2[79],0,140)
# calculate_TZ(OLIG2[79],0,140,OLIG2_V)

# plot_single_profile(OLIG2[80],0,150)
# calculate_TZ(OLIG2[80],0,150,OLIG2_V)

# # plot_single_profile(OLIG2[81],20,50)
# # calculate_TZ(OLIG2[81],20,50,OLIG2_V)

# # plot_single_profile(OLIG2[82],10,80)
# # calculate_TZ(OLIG2[82],10,80,OLIG2_V)

# # plot_single_profile(OLIG2[83],0,500)
# # calculate_TZ(OLIG2[83],40,100,OLIG2_V)

# # plot_single_profile(OLIG2[84],0,500)
# # calculate_TZ(OLIG2[84],40,50,OLIG2_V)

# plot_single_profile(OLIG2[85],0,140)
# calculate_TZ(OLIG2[85],0,140,OLIG2_V)

# plot_single_profile(OLIG2[86],0,150)
# calculate_TZ(OLIG2[86],0,150,OLIG2_V)

# plot_single_profile(OLIG2[87],0,150)
# calculate_TZ(OLIG2[87],0,150,OLIG2_V)

# plot_single_profile(OLIG2[88],0,100)
# calculate_TZ(OLIG2[88],0,100,OLIG2_V)

# plot_single_profile(OLIG2[89],0,120)
# calculate_TZ(OLIG2[89],0,120,OLIG2_V)

# plot_single_profile(OLIG2[90],0,100)
# calculate_TZ(OLIG2[90],0,100,OLIG2_V)

# plot_single_profile(OLIG2[91],50,100)
# calculate_TZ(OLIG2[91],50,100,OLIG2_V)

# plot_single_profile(OLIG2[92],0,140)
# calculate_TZ(OLIG2[92],0,140,OLIG2_V)

# plot_single_profile(OLIG2[93],0,120)
# calculate_TZ(OLIG2[93],0,120,OLIG2_V)

# # plot_single_profile(OLIG2[94],0,90)
# # calculate_TZ(OLIG2[94],0,120,OLIG2_V)

# plot_single_profile(OLIG2[95],0,120)
# calculate_TZ(OLIG2[95],0,120,OLIG2_V)

# plot_single_profile(OLIG2[96],0,120)
# calculate_TZ(OLIG2[96],0,120,OLIG2_V)

# plot_single_profile(OLIG2[97],0,150)
# calculate_TZ(OLIG2[97],0,150,OLIG2_V)

# plot_single_profile(OLIG2[98],0,120)
# calculate_TZ(OLIG2[98],0,120,OLIG2_V)

# plot_single_profile(OLIG2[99],0,140)
# calculate_TZ(OLIG2[99],0,140,OLIG2_V)

# plot_single_profile(OLIG2[100],0,160)
# calculate_TZ(OLIG2[100],0,160,OLIG2_V)


# # OLIG2_D


# # plot_single_profile(OLIG2[1],0,500)
# # calculate_TZ(OLIG2[1],0,500,OLIG2_D)

# # plot_single_profile(OLIG2[2],50,500)
# # calculate_TZ(OLIG2[2],50,500,OLIG2_D)

# plot_single_profile(OLIG2[3],60,300)
# calculate_TZ(OLIG2[3],60,300,OLIG2_D)

# plot_single_profile(OLIG2[4],60,200)
# calculate_TZ(OLIG2[4],60,200,OLIG2_D)

# plot_single_profile(OLIG2[5],70,200)
# calculate_TZ(OLIG2[5],70,200,OLIG2_D)

# plot_single_profile(OLIG2[6],100,200)
# calculate_TZ(OLIG2[6],100,200,OLIG2_D)

# plot_single_profile(OLIG2[7],120,140)
# calculate_TZ(OLIG2[7],120,140,OLIG2_D)

# plot_single_profile(OLIG2[8],60,200)
# calculate_TZ(OLIG2[8],60,200,OLIG2_D)

# plot_single_profile(OLIG2[9],80,200)
# calculate_TZ(OLIG2[9],80,200,OLIG2_D)

# plot_single_profile(OLIG2[10],100,200)
# calculate_TZ(OLIG2[10],100,200,OLIG2_D)

# plot_single_profile(OLIG2[11],100,200)
# calculate_TZ(OLIG2[11],100,200,OLIG2_D)

# plot_single_profile(OLIG2[12],100,200)
# calculate_TZ(OLIG2[12],100,200,OLIG2_D)

# plot_single_profile(OLIG2[13],70,300)
# calculate_TZ(OLIG2[13],70,300,OLIG2_D)

# plot_single_profile(OLIG2[14],50,300)
# calculate_TZ(OLIG2[14],50,300,OLIG2_D)

# plot_single_profile(OLIG2[15],110,200)
# calculate_TZ(OLIG2[15],110,200,OLIG2_D)

# plot_single_profile(OLIG2[16],50,300)
# calculate_TZ(OLIG2[16],50,300,OLIG2_D)

# plot_single_profile(OLIG2[17],60,200)
# calculate_TZ(OLIG2[17],60,200,OLIG2_D)

# plot_single_profile(OLIG2[18],80,200)
# calculate_TZ(OLIG2[18],80,200,OLIG2_D)

# plot_single_profile(OLIG2[19],90,200)
# calculate_TZ(OLIG2[19],90,200,OLIG2_D)

# plot_single_profile(OLIG2[20],110,200)
# calculate_TZ(OLIG2[20],110,200,OLIG2_D)

# plot_single_profile(OLIG2[21],65,200)
# calculate_TZ(OLIG2[21],65,200,OLIG2_D)

# plot_single_profile(OLIG2[22],80,200)
# calculate_TZ(OLIG2[22],80,200,OLIG2_D)

# plot_single_profile(OLIG2[23],70,200)
# calculate_TZ(OLIG2[23],70,200,OLIG2_D)

# plot_single_profile(OLIG2[24],70,200)
# calculate_TZ(OLIG2[24],70,200,OLIG2_D)

# plot_single_profile(OLIG2[25],60,200)
# calculate_TZ(OLIG2[25],60,200,OLIG2_D)

# plot_single_profile(OLIG2[26],60,200)
# calculate_TZ(OLIG2[26],60,200,OLIG2_D)

# plot_single_profile(OLIG2[27],60,200)
# calculate_TZ(OLIG2[27],60,200,OLIG2_D)

# plot_single_profile(OLIG2[28],60,200)
# calculate_TZ(OLIG2[28],60,200,OLIG2_D)

# plot_single_profile(OLIG2[29],60,200)
# calculate_TZ(OLIG2[29],60,200,OLIG2_D)

# plot_single_profile(OLIG2[30],60,200)
# calculate_TZ(OLIG2[30],60,200,OLIG2_D)

# plot_single_profile(OLIG2[31],60,200)
# calculate_TZ(OLIG2[31],60,200,OLIG2_D)

# plot_single_profile(OLIG2[32],60,200)
# calculate_TZ(OLIG2[32],60,200,OLIG2_D)

# plot_single_profile(OLIG2[33],60,200)
# calculate_TZ(OLIG2[33],60,200,OLIG2_D)

# plot_single_profile(OLIG2[34],60,200)
# calculate_TZ(OLIG2[34],60,200,OLIG2_D)

# plot_single_profile(OLIG2[35],60,200)
# calculate_TZ(OLIG2[35],60,200,OLIG2_D)

# plot_single_profile(OLIG2[36],50,200)
# calculate_TZ(OLIG2[36],50,200,OLIG2_D)

# plot_single_profile(OLIG2[37],60,200)
# calculate_TZ(OLIG2[37],60,200,OLIG2_D)

# plot_single_profile(OLIG2[38],60,200)
# calculate_TZ(OLIG2[38],60,200,OLIG2_D)

# plot_single_profile(OLIG2[39],60,200)
# calculate_TZ(OLIG2[39],60,200,OLIG2_D)

# plot_single_profile(OLIG2[40],60,200)
# calculate_TZ(OLIG2[40],60,200,OLIG2_D)

# plot_single_profile(OLIG2[41],70,200)
# calculate_TZ(OLIG2[41],70,200,OLIG2_D)

# plot_single_profile(OLIG2[42],70,200)
# calculate_TZ(OLIG2[42],70,200,OLIG2_D)

# plot_single_profile(OLIG2[43],70,200)
# calculate_TZ(OLIG2[43],70,200,OLIG2_D)

# plot_single_profile(OLIG2[44],70,200)
# calculate_TZ(OLIG2[44],70,200,OLIG2_D)

# plot_single_profile(OLIG2[45],80,200)
# calculate_TZ(OLIG2[45],60,200,OLIG2_D)

# plot_single_profile(OLIG2[46],60,200)
# calculate_TZ(OLIG2[46],60,200,OLIG2_D)

# plot_single_profile(OLIG2[47],60,200)
# calculate_TZ(OLIG2[47],60,200,OLIG2_D)

# plot_single_profile(OLIG2[48],60,200)
# calculate_TZ(OLIG2[48],60,200,OLIG2_D)

# plot_single_profile(OLIG2[49],60,200)
# calculate_TZ(OLIG2[49],60,200,OLIG2_D)

# plot_single_profile(OLIG2[50],60,200)
# calculate_TZ(OLIG2[50],60,200,OLIG2_D)

# plot_single_profile(OLIG2[51],60,200)
# calculate_TZ(OLIG2[51],60,200,OLIG2_D)

# plot_single_profile(OLIG2[52],60,200)
# calculate_TZ(OLIG2[52],60,200,OLIG2_D)

# plot_single_profile(OLIG2[53],60,200)
# calculate_TZ(OLIG2[53],60,200,OLIG2_D)

# plot_single_profile(OLIG2[54],60,200)
# calculate_TZ(OLIG2[54],60,200,OLIG2_D)

# plot_single_profile(OLIG2[55],60,200)
# calculate_TZ(OLIG2[55],60,200,OLIG2_D)

# plot_single_profile(OLIG2[56],60,200)
# calculate_TZ(OLIG2[56],60,200,OLIG2_D)

# plot_single_profile(OLIG2[57],60,200)
# calculate_TZ(OLIG2[57],60,200,OLIG2_D)

# plot_single_profile(OLIG2[58],60,200)
# calculate_TZ(OLIG2[58],60,200,OLIG2_D)

# plot_single_profile(OLIG2[59],60,200)
# calculate_TZ(OLIG2[59],60,200,OLIG2_D)

# plot_single_profile(OLIG2[60],60,200)
# calculate_TZ(OLIG2[60],60,200,OLIG2_D)

# # plot_single_profile(OLIG2[61],60,200)
# # calculate_TZ(OLIG2[61],60,200,OLIG2_D)

# plot_single_profile(OLIG2[62],60,200)
# calculate_TZ(OLIG2[62],60,200,OLIG2_D)

# plot_single_profile(OLIG2[63],60,200)
# calculate_TZ(OLIG2[63],60,200,OLIG2_D)

# plot_single_profile(OLIG2[64],60,200)
# calculate_TZ(OLIG2[64],60,200,OLIG2_D)

# plot_single_profile(OLIG2[65],60,200)
# calculate_TZ(OLIG2[65],60,200,OLIG2_D)

# plot_single_profile(OLIG2[66],60,200)
# calculate_TZ(OLIG2[66],60,200,OLIG2_D)

# plot_single_profile(OLIG2[67],70,200)
# calculate_TZ(OLIG2[67],70,200,OLIG2_D)

# plot_single_profile(OLIG2[68],70,200)
# calculate_TZ(OLIG2[68],70,200,OLIG2_D)

# plot_single_profile(OLIG2[69],70,200)
# calculate_TZ(OLIG2[69],70,200,OLIG2_D)

# plot_single_profile(OLIG2[70],70,200)
# calculate_TZ(OLIG2[70],70,200,OLIG2_D)

# plot_single_profile(OLIG2[71],70,200)
# calculate_TZ(OLIG2[71],70,200,OLIG2_D)

# plot_single_profile(OLIG2[72],70,200)
# calculate_TZ(OLIG2[72],70,200,OLIG2_D)

# plot_single_profile(OLIG2[73],70,200)
# calculate_TZ(OLIG2[73],70,200,OLIG2_D)

# # plot_single_profile(OLIG2[74],150,250)
# # calculate_TZ(OLIG2[74],150,250,OLIG2_D)

# plot_single_profile(OLIG2[75],90,500)
# calculate_TZ(OLIG2[75],90,500,OLIG2_D)

# plot_single_profile(OLIG2[76],120,200)
# calculate_TZ(OLIG2[76],120,200,OLIG2_D)

# plot_single_profile(OLIG2[77],100,300)
# calculate_TZ(OLIG2[77],100,300,OLIG2_D)

# plot_single_profile(OLIG2[78],100,300)
# calculate_TZ(OLIG2[78],100,300,OLIG2_D)

# plot_single_profile(OLIG2[79],100,300)
# calculate_TZ(OLIG2[79],100,300,OLIG2_D)

# plot_single_profile(OLIG2[80],100,300)
# calculate_TZ(OLIG2[80],100,300,OLIG2_D)

# plot_single_profile(OLIG2[81],100,300)
# calculate_TZ(OLIG2[81],100,300,OLIG2_D)

# plot_single_profile(OLIG2[82],50,300)
# calculate_TZ(OLIG2[82],50,300,OLIG2_D)

# plot_single_profile(OLIG2[83],50,300)
# calculate_TZ(OLIG2[83],50,300,OLIG2_D)

# plot_single_profile(OLIG2[84],50,300)
# calculate_TZ(OLIG2[84],50,300,OLIG2_D)

# plot_single_profile(OLIG2[85],50,300)
# calculate_TZ(OLIG2[85],50,300,OLIG2_D)

# plot_single_profile(OLIG2[86],50,300)
# calculate_TZ(OLIG2[86],50,300,OLIG2_D)

# plot_single_profile(OLIG2[87],50,300)
# calculate_TZ(OLIG2[87],50,300,OLIG2_D)

# plot_single_profile(OLIG2[88],50,300)
# calculate_TZ(OLIG2[88],50,300,OLIG2_D)

# plot_single_profile(OLIG2[89],50,300)
# calculate_TZ(OLIG2[89],50,300,OLIG2_D)

# plot_single_profile(OLIG2[90],50,300)
# calculate_TZ(OLIG2[90],50,300,OLIG2_D)

# plot_single_profile(OLIG2[91],50,300)
# calculate_TZ(OLIG2[91],50,300,OLIG2_D)

# plot_single_profile(OLIG2[92],50,300)
# calculate_TZ(OLIG2[92],50,300,OLIG2_D)

# plot_single_profile(OLIG2[93],50,300)
# calculate_TZ(OLIG2[93],50,300,OLIG2_D)

# plot_single_profile(OLIG2[94],50,300)
# calculate_TZ(OLIG2[94],50,300,OLIG2_D)

# plot_single_profile(OLIG2[95],50,300)
# calculate_TZ(OLIG2[95],50,300,OLIG2_D)

# plot_single_profile(OLIG2[96],50,300)
# calculate_TZ(OLIG2[96],50,300,OLIG2_D)

# plot_single_profile(OLIG2[97],50,300)
# calculate_TZ(OLIG2[97],50,300,OLIG2_D)

# plot_single_profile(OLIG2[98],100,300)
# calculate_TZ(OLIG2[98],100,300,OLIG2_D)

# plot_single_profile(OLIG2[99],100,300)
# calculate_TZ(OLIG2[99],100,300,OLIG2_D)

# plot_single_profile(OLIG2[100],100,300)
# calculate_TZ(OLIG2[100],100,300,OLIG2_D)


# #  OLIG3_V


# plot_single_profile(OLIG3[1],150,300)
# calculate_TZ(OLIG3[1],150,300,OLIG3_V)

# plot_single_profile(OLIG3[2],150,300)
# calculate_TZ(OLIG3[2],150,300,OLIG3_V)

# plot_single_profile(OLIG3[3],150,300)
# calculate_TZ(OLIG3[3],150,300,OLIG3_V)

# plot_single_profile(OLIG3[4],150,300)
# calculate_TZ(OLIG3[4],150,300,OLIG3_V)

# plot_single_profile(OLIG3[5],150,300)
# calculate_TZ(OLIG3[5],150,300,OLIG3_V)

# plot_single_profile(OLIG3[6],150,300)
# calculate_TZ(OLIG3[6],150,300,OLIG3_V)

# plot_single_profile(OLIG3[7],150,300)
# calculate_TZ(OLIG3[7],150,300,OLIG3_V)

# plot_single_profile(OLIG3[8],150,300)
# calculate_TZ(OLIG3[8],150,300,OLIG3_V)

# plot_single_profile(OLIG3[9],150,300)
# calculate_TZ(OLIG3[9],150,300,OLIG3_V)

# plot_single_profile(OLIG3[10],150,300)
# calculate_TZ(OLIG3[10],150,300,OLIG3_V)

# plot_single_profile(OLIG3[11],150,300)
# calculate_TZ(OLIG3[11],150,300,OLIG3_V)

# plot_single_profile(OLIG3[12],150,300)
# calculate_TZ(OLIG3[12],150,300,OLIG3_V)

# plot_single_profile(OLIG3[13],150,300)
# calculate_TZ(OLIG3[13],150,300,OLIG3_V)

# plot_single_profile(OLIG3[14],100,300)
# calculate_TZ(OLIG3[14],100,300,OLIG3_V)

# plot_single_profile(OLIG3[15],0,400)
# calculate_TZ(OLIG3[15],0,400,OLIG3_V)

# plot_single_profile(OLIG3[16],0,400)
# calculate_TZ(OLIG3[16],0,400,OLIG3_V)

# plot_single_profile(OLIG3[17],150,400)
# calculate_TZ(OLIG3[17],150,400,OLIG3_V)

# plot_single_profile(OLIG3[18],150,400)
# calculate_TZ(OLIG3[18],150,400,OLIG3_V)

# plot_single_profile(OLIG3[19],150,400)
# calculate_TZ(OLIG3[19],150,400,OLIG3_V)

# plot_single_profile(OLIG3[20],150,400)
# calculate_TZ(OLIG3[20],150,400,OLIG3_V)

# plot_single_profile(OLIG3[21],150,400)
# calculate_TZ(OLIG3[21],150,400,OLIG3_V)

# plot_single_profile(OLIG3[22],150,400)
# calculate_TZ(OLIG3[22],150,400,OLIG3_V)

# plot_single_profile(OLIG3[23],150,400)
# calculate_TZ(OLIG3[23],150,400,OLIG3_V)

# plot_single_profile(OLIG3[24],150,400)
# calculate_TZ(OLIG3[24],150,400,OLIG3_V)

# plot_single_profile(OLIG3[25],150,400)
# calculate_TZ(OLIG3[25],150,400,OLIG3_V)

# plot_single_profile(OLIG3[26],150,400)
# calculate_TZ(OLIG3[26],150,400,OLIG3_V)

# plot_single_profile(OLIG3[27],150,400)
# calculate_TZ(OLIG3[27],150,400,OLIG3_V)

# plot_single_profile(OLIG3[28],150,400)
# calculate_TZ(OLIG3[28],150,400,OLIG3_V)

# plot_single_profile(OLIG3[29],150,400)
# calculate_TZ(OLIG3[29],150,400,OLIG3_V)

# plot_single_profile(OLIG3[30],150,400)
# calculate_TZ(OLIG3[30],150,400,OLIG3_V)

# plot_single_profile(OLIG3[31],150,400)
# calculate_TZ(OLIG3[31],150,400,OLIG3_V)

# plot_single_profile(OLIG3[32],150,400)
# calculate_TZ(OLIG3[32],150,400,OLIG3_V)

# plot_single_profile(OLIG3[33],150,400)
# calculate_TZ(OLIG3[33],150,400,OLIG3_V)

# plot_single_profile(OLIG3[34],150,400)
# calculate_TZ(OLIG3[34],150,400,OLIG3_V)

# plot_single_profile(OLIG3[35],150,400)
# calculate_TZ(OLIG3[35],150,400,OLIG3_V)

# plot_single_profile(OLIG3[36],150,400)
# calculate_TZ(OLIG3[36],150,400,OLIG3_V)

# plot_single_profile(OLIG3[37],150,400)
# calculate_TZ(OLIG3[37],150,400,OLIG3_V)

# plot_single_profile(OLIG3[38],100,400)
# calculate_TZ(OLIG3[38],100,400,OLIG3_V)

# plot_single_profile(OLIG3[39],100,400)
# calculate_TZ(OLIG3[39],100,400,OLIG3_V)

# plot_single_profile(OLIG3[40],100,400)
# calculate_TZ(OLIG3[40],100,400,OLIG3_V)

# plot_single_profile(OLIG3[41],100,400)
# calculate_TZ(OLIG3[41],100,400,OLIG3_V)

# plot_single_profile(OLIG3[42],150,400)
# calculate_TZ(OLIG3[42],150,400,OLIG3_V)

# plot_single_profile(OLIG3[43],150,270)
# calculate_TZ(OLIG3[43],150,270,OLIG3_V)

# plot_single_profile(OLIG3[44],150,400)
# calculate_TZ(OLIG3[44],150,400,OLIG3_V)

# plot_single_profile(OLIG3[45],150,400)
# calculate_TZ(OLIG3[45],150,400,OLIG3_V)

# plot_single_profile(OLIG3[46],150,400)
# calculate_TZ(OLIG3[46],150,400,OLIG3_V)

# plot_single_profile(OLIG3[47],150,400)
# calculate_TZ(OLIG3[47],150,400,OLIG3_V)

# plot_single_profile(OLIG3[48],150,400)
# calculate_TZ(OLIG3[48],150,400,OLIG3_V)

# plot_single_profile(OLIG3[49],150,400)
# calculate_TZ(OLIG3[49],150,400,OLIG3_V)

# plot_single_profile(OLIG3[50],150,400)
# calculate_TZ(OLIG3[50],150,400,OLIG3_V)

# plot_single_profile(OLIG3[51],150,400)
# calculate_TZ(OLIG3[51],150,400,OLIG3_V)

# plot_single_profile(OLIG3[52],150,400)
# calculate_TZ(OLIG3[52],150,400,OLIG3_V)

# plot_single_profile(OLIG3[53],150,400)
# calculate_TZ(OLIG3[53],150,400,OLIG3_V)

# plot_single_profile(OLIG3[54],150,400)
# calculate_TZ(OLIG3[54],150,400,OLIG3_V)

# plot_single_profile(OLIG3[55],150,400)
# calculate_TZ(OLIG3[55],150,400,OLIG3_V)

# plot_single_profile(OLIG3[56],150,400)
# calculate_TZ(OLIG3[56],150,400,OLIG3_V)

# plot_single_profile(OLIG3[57],150,400)
# calculate_TZ(OLIG3[57],150,400,OLIG3_V)

# plot_single_profile(OLIG3[58],150,400)
# calculate_TZ(OLIG3[58],150,400,OLIG3_V)

# plot_single_profile(OLIG3[59],150,400)
# calculate_TZ(OLIG3[59],150,400,OLIG3_V)

# plot_single_profile(OLIG3[60],100,400)
# calculate_TZ(OLIG3[60],100,400,OLIG3_V)

# plot_single_profile(OLIG3[61],100,400)
# calculate_TZ(OLIG3[61],100,400,OLIG3_V)

# plot_single_profile(OLIG3[62],100,400)
# calculate_TZ(OLIG3[62],100,400,OLIG3_V)

# plot_single_profile(OLIG3[63],100,400)
# calculate_TZ(OLIG3[63],100,400,OLIG3_V)

# plot_single_profile(OLIG3[64],100,400)
# calculate_TZ(OLIG3[64],100,400,OLIG3_V)

# plot_single_profile(OLIG3[65],100,400)
# calculate_TZ(OLIG3[65],100,400,OLIG3_V)

# plot_single_profile(OLIG3[66],100,400)
# calculate_TZ(OLIG3[66],100,400,OLIG3_V)

# plot_single_profile(OLIG3[67],100,400)
# calculate_TZ(OLIG3[67],100,400,OLIG3_V)

# plot_single_profile(OLIG3[68],100,400)
# calculate_TZ(OLIG3[68],100,400,OLIG3_V)

# plot_single_profile(OLIG3[69],100,400)
# calculate_TZ(OLIG3[69],100,400,OLIG3_V)

# plot_single_profile(OLIG3[70],100,400)
# calculate_TZ(OLIG3[70],100,400,OLIG3_V)

# plot_single_profile(OLIG3[71],100,400)
# calculate_TZ(OLIG3[71],100,400,OLIG3_V)

# plot_single_profile(OLIG3[72],100,400)
# calculate_TZ(OLIG3[72],100,400,OLIG3_V)

# plot_single_profile(OLIG3[73],100,400)
# calculate_TZ(OLIG3[73],100,400,OLIG3_V)

# plot_single_profile(OLIG3[74],100,400)
# calculate_TZ(OLIG3[74],100,400,OLIG3_V)

# plot_single_profile(OLIG3[75],100,400)
# calculate_TZ(OLIG3[75],100,400,OLIG3_V)

# plot_single_profile(OLIG3[76],100,400)
# calculate_TZ(OLIG3[76],100,400,OLIG3_V)

# plot_single_profile(OLIG3[77],100,400)
# calculate_TZ(OLIG3[77],100,400,OLIG3_V)

# plot_single_profile(OLIG3[78],100,400)
# calculate_TZ(OLIG3[78],100,400,OLIG3_V)

# plot_single_profile(OLIG3[79],100,400)
# calculate_TZ(OLIG3[79],100,400,OLIG3_V)

# plot_single_profile(OLIG3[80],100,400)
# calculate_TZ(OLIG3[80],100,400,OLIG3_V)

# plot_single_profile(OLIG3[81],100,400)
# calculate_TZ(OLIG3[81],100,400,OLIG3_V)

# plot_single_profile(OLIG3[82],100,400)
# calculate_TZ(OLIG3[82],100,400,OLIG3_V)

# plot_single_profile(OLIG3[83],100,400)
# calculate_TZ(OLIG3[83],100,400,OLIG3_V)

# plot_single_profile(OLIG3[84],100,400)
# calculate_TZ(OLIG3[84],100,400,OLIG3_V)

# plot_single_profile(OLIG3[85],100,400)
# calculate_TZ(OLIG3[85],100,400,OLIG3_V)

# plot_single_profile(OLIG3[86],100,260)
# calculate_TZ(OLIG3[86],100,260,OLIG3_V)

# plot_single_profile(OLIG3[87],100,280)
# calculate_TZ(OLIG3[87],100,280,OLIG3_V)

# plot_single_profile(OLIG3[88],100,400)
# calculate_TZ(OLIG3[88],100,400,OLIG3_V)

# plot_single_profile(OLIG3[89],400,500)
# calculate_TZ(OLIG3[89],400,500,OLIG3_V)

# plot_single_profile(OLIG3[90],400,500)
# calculate_TZ(OLIG3[90],400,500,OLIG3_V)

# plot_single_profile(OLIG3[91],400,550)
# calculate_TZ(OLIG3[91],400,550,OLIG3_V)

# plot_single_profile(OLIG3[92],400,550)
# calculate_TZ(OLIG3[92],400,550,OLIG3_V)

# plot_single_profile(OLIG3[93],350,450)
# calculate_TZ(OLIG3[93],350,450,OLIG3_V)

# plot_single_profile(OLIG3[94],350,450)
# calculate_TZ(OLIG3[94],350,450,OLIG3_V)

# plot_single_profile(OLIG3[95],350,600)
# calculate_TZ(OLIG3[95],350,600,OLIG3_V)

# plot_single_profile(OLIG3[96],300,600)
# calculate_TZ(OLIG3[96],300,600,OLIG3_V)

# plot_single_profile(OLIG3[97],300,600)
# calculate_TZ(OLIG3[97],300,600,OLIG3_V)

# plot_single_profile(OLIG3[98],350,600)
# calculate_TZ(OLIG3[98],350,600,OLIG3_V)

# plot_single_profile(OLIG3[99],300,600)
# calculate_TZ(OLIG3[99],300,600,OLIG3_V)

# plot_single_profile(OLIG3[100],400,520)
# calculate_TZ(OLIG3[100],400,520,OLIG3_V)

# plot_single_profile(OLIG3[101],430,510)
# calculate_TZ(OLIG3[101],430,510,OLIG3_V)

# plot_single_profile(OLIG3[102],420,510)
# calculate_TZ(OLIG3[102],420,510,OLIG3_V)

# plot_single_profile(OLIG3[103],420,530)
# calculate_TZ(OLIG3[103],420,530,OLIG3_V)

# plot_single_profile(OLIG3[104],450,520)
# calculate_TZ(OLIG3[104],450,520,OLIG3_V)

# plot_single_profile(OLIG3[105],450,520)
# calculate_TZ(OLIG3[105],450,520,OLIG3_V)

# plot_single_profile(OLIG3[106],450,520)
# calculate_TZ(OLIG3[106],450,520,OLIG3_V)

# plot_single_profile(OLIG3[107],450,700)
# calculate_TZ(OLIG3[107],450,700,OLIG3_V)

# plot_single_profile(OLIG3[108],450,700)
# calculate_TZ(OLIG3[108],450,700,OLIG3_V)

# plot_single_profile(OLIG3[109],400,700)
# calculate_TZ(OLIG3[109],400,700,OLIG3_V)

# plot_single_profile(OLIG3[110],0,700)
# calculate_TZ(OLIG3[109],400,700,OLIG3_V)

# plot_single_profile(OLIG3[111],300,565)
# calculate_TZ(OLIG3[111],300,565,OLIG3_V)

# plot_single_profile(OLIG3[112],400,700)
# calculate_TZ(OLIG3[112],400,700,OLIG3_V)



# # PAX6_V

# plot_single_profile(PAX6[1],0,230)
# calculate_TZ(PAX6[1],0,230,PAX6_V)

# plot_single_profile(PAX6[2],0,250)
# calculate_TZ(PAX6[2],0,250,PAX6_V)

# plot_single_profile(PAX6[3],0,200)
# calculate_TZ(PAX6[3],0,200,PAX6_V)

# plot_single_profile(PAX6[4],0,200)
# calculate_TZ(PAX6[4],0,200,PAX6_V)

# plot_single_profile(PAX6[5],0,200)
# calculate_TZ(PAX6[5],0,200,PAX6_V)

# plot_single_profile(PAX6[6],0,200)
# calculate_TZ(PAX6[6],0,200,PAX6_V)

# # plot_single_profile(PAX6[7],0,450)
# # calculate_TZ(PAX6[7],0,70,PAX6_V)

# plot_single_profile(PAX6[8],0,350)
# calculate_TZ(PAX6[8],0,350,PAX6_V)

# plot_single_profile(PAX6[9],0,200)
# calculate_TZ(PAX6[9],0,200,PAX6_V)

# plot_single_profile(PAX6[10],0,200)
# calculate_TZ(PAX6[10],0,200,PAX6_V)

# plot_single_profile(PAX6[11],0,200)
# calculate_TZ(PAX6[11],0,200,PAX6_V)

# plot_single_profile(PAX6[12],0,350)
# calculate_TZ(PAX6[12],0,350,PAX6_V)

# plot_single_profile(PAX6[13],0,200)
# calculate_TZ(PAX6[13],0,200,PAX6_V)

# plot_single_profile(PAX6[14],0,200)
# calculate_TZ(PAX6[14],0,200,PAX6_V)

# plot_single_profile(PAX6[15],0,200)
# calculate_TZ(PAX6[15],0,200,PAX6_V)

# plot_single_profile(PAX6[16],0,250)
# calculate_TZ(PAX6[16],0,250,PAX6_V)

# plot_single_profile(PAX6[17],0,250)
# calculate_TZ(PAX6[17],0,250,PAX6_V)

# plot_single_profile(PAX6[18],0,250)
# calculate_TZ(PAX6[18],0,250,PAX6_V)

# plot_single_profile(PAX6[19],0,250)
# calculate_TZ(PAX6[19],0,250,PAX6_V)

# plot_single_profile(PAX6[20],0,200)
# calculate_TZ(PAX6[20],0,200,PAX6_V)

# plot_single_profile(PAX6[21],0,250)
# calculate_TZ(PAX6[21],0,250,PAX6_V)

# plot_single_profile(PAX6[22],0,200)
# calculate_TZ(PAX6[22],0,200,PAX6_V)

# plot_single_profile(PAX6[23],0,200)
# calculate_TZ(PAX6[23],0,200,PAX6_V)

# plot_single_profile(PAX6[24],0,200)
# calculate_TZ(PAX6[24],0,200,PAX6_V)

# plot_single_profile(PAX6[25],0,250)
# calculate_TZ(PAX6[25],0,250,PAX6_V)

# plot_single_profile(PAX6[26],0,250)
# calculate_TZ(PAX6[26],0,250,PAX6_V)

# plot_single_profile(PAX6[27],0,300)
# calculate_TZ(PAX6[27],0,300,PAX6_V)

# plot_single_profile(PAX6[28],0,250)
# calculate_TZ(PAX6[28],0,250,PAX6_V)

# plot_single_profile(PAX6[29],0,300)
# calculate_TZ(PAX6[29],0,300,PAX6_V)

# plot_single_profile(PAX6[30],0,250)
# calculate_TZ(PAX6[30],0,250,PAX6_V)

# plot_single_profile(PAX6[31],0,250)
# calculate_TZ(PAX6[31],0,250,PAX6_V)

# plot_single_profile(PAX6[32],0,350)
# calculate_TZ(PAX6[32],0,350,PAX6_V)

# plot_single_profile(PAX6[33],0,350)
# calculate_TZ(PAX6[33],0,350,PAX6_V)

# plot_single_profile(PAX6[34],0,350)
# calculate_TZ(PAX6[34],0,350,PAX6_V)

# plot_single_profile(PAX6[35],0,350)
# calculate_TZ(PAX6[35],0,350,PAX6_V)

# plot_single_profile(PAX6[36],0,250)
# calculate_TZ(PAX6[36],0,250,PAX6_V)

# plot_single_profile(PAX6[37],0,350)
# calculate_TZ(PAX6[37],0,350,PAX6_V)

# plot_single_profile(PAX6[38],0,200)
# calculate_TZ(PAX6[38],0,200,PAX6_V)

# plot_single_profile(PAX6[39],0,350)
# calculate_TZ(PAX6[39],0,350,PAX6_V)

# plot_single_profile(PAX6[40],0,250)
# calculate_TZ(PAX6[40],0,250,PAX6_V)

# plot_single_profile(PAX6[41],0,350)
# calculate_TZ(PAX6[41],0,350,PAX6_V)

# plot_single_profile(PAX6[42],0,200)
# calculate_TZ(PAX6[42],0,200,PAX6_V)

# plot_single_profile(PAX6[43],0,350)
# calculate_TZ(PAX6[43],0,350,PAX6_V)

# plot_single_profile(PAX6[44],0,350)
# calculate_TZ(PAX6[44],0,350,PAX6_V)

# plot_single_profile(PAX6[45],0,350)
# calculate_TZ(PAX6[45],0,350,PAX6_V)

# plot_single_profile(PAX6[46],0,350)
# calculate_TZ(PAX6[46],0,350,PAX6_V)

# plot_single_profile(PAX6[47],0,350)
# calculate_TZ(PAX6[47],0,350,PAX6_V)

# plot_single_profile(PAX6[48],0,250)
# calculate_TZ(PAX6[48],0,250,PAX6_V)

# plot_single_profile(PAX6[49],0,350)
# calculate_TZ(PAX6[49],0,350,PAX6_V)

# plot_single_profile(PAX6[50],0,350)
# calculate_TZ(PAX6[50],0,350,PAX6_V)

# plot_single_profile(PAX6[51],0,250)
# calculate_TZ(PAX6[51],0,250,PAX6_V)

# plot_single_profile(PAX6[52],0,200)
# calculate_TZ(PAX6[52],0,250,PAX6_V)

# plot_single_profile(PAX6[53],0,150)
# calculate_TZ(PAX6[53],0,150,PAX6_V)

# plot_single_profile(PAX6[54],0,160)
# calculate_TZ(PAX6[54],0,160,PAX6_V)

# plot_single_profile(PAX6[55],0,200)
# calculate_TZ(PAX6[55],0,200,PAX6_V)

# plot_single_profile(PAX6[56],0,200)
# calculate_TZ(PAX6[56],0,200,PAX6_V)

# plot_single_profile(PAX6[57],0,190)
# calculate_TZ(PAX6[57],0,190,PAX6_V)

# plot_single_profile(PAX6[58],0,190)
# calculate_TZ(PAX6[58],0,190,PAX6_V)

# plot_single_profile(PAX6[59],0,150)
# calculate_TZ(PAX6[59],0,150,PAX6_V)

# plot_single_profile(PAX6[60],0,350)
# calculate_TZ(PAX6[60],0,350,PAX6_V)

# plot_single_profile(PAX6[61],0,250)
# calculate_TZ(PAX6[61],0,250,PAX6_V)

# plot_single_profile(PAX6[62],0,250)
# calculate_TZ(PAX6[62],0,250,PAX6_V)

# plot_single_profile(PAX6[63],0,250)
# calculate_TZ(PAX6[63],0,250,PAX6_V)

# plot_single_profile(PAX6[64],0,250)
# calculate_TZ(PAX6[64],0,250,PAX6_V)

# plot_single_profile(PAX6[65],0,150)
# calculate_TZ(PAX6[65],0,150,PAX6_V)

# plot_single_profile(PAX6[66],0,150)
# calculate_TZ(PAX6[66],0,150,PAX6_V)

# plot_single_profile(PAX6[67],0,250)
# calculate_TZ(PAX6[67],0,250,PAX6_V)

# plot_single_profile(PAX6[68],0,250)
# calculate_TZ(PAX6[68],0,250,PAX6_V)

# plot_single_profile(PAX6[69],0,250)
# calculate_TZ(PAX6[69],0,250,PAX6_V)

# plot_single_profile(PAX6[70],0,250)
# calculate_TZ(PAX6[70],0,250,PAX6_V)

# plot_single_profile(PAX6[71],0,150)
# calculate_TZ(PAX6[71],0,150,PAX6_V)

# plot_single_profile(PAX6[72],0,250)
# calculate_TZ(PAX6[72],0,250,PAX6_V)

# plot_single_profile(PAX6[73],0,250)
# calculate_TZ(PAX6[73],0,250,PAX6_V)

# plot_single_profile(PAX6[74],0,250)
# calculate_TZ(PAX6[74],0,250,PAX6_V)

# plot_single_profile(PAX6[75],0,250)
# calculate_TZ(PAX6[75],0,250,PAX6_V)

# plot_single_profile(PAX6[76],0,250)
# calculate_TZ(PAX6[76],0,250,PAX6_V)

# plot_single_profile(PAX6[77],0,150)
# calculate_TZ(PAX6[77],0,150,PAX6_V)

# plot_single_profile(PAX6[78],0,150)
# calculate_TZ(PAX6[78],0,150,PAX6_V)

# plot_single_profile(PAX6[79],0,250)
# calculate_TZ(PAX6[79],0,250,PAX6_V)

# plot_single_profile(PAX6[80],0,250)
# calculate_TZ(PAX6[80],0,250,PAX6_V)

# plot_single_profile(PAX6[81],0,200)
# calculate_TZ(PAX6[81],0,200,PAX6_V)

# plot_single_profile(PAX6[82],0,200)
# calculate_TZ(PAX6[82],0,200,PAX6_V)

# plot_single_profile(PAX6[83],0,200)
# calculate_TZ(PAX6[83],0,200,PAX6_V)

# plot_single_profile(PAX6[84],0,200)
# calculate_TZ(PAX6[84],0,200,PAX6_V)

# plot_single_profile(PAX6[85],0,200)
# calculate_TZ(PAX6[85],0,200,PAX6_V)

# plot_single_profile(PAX6[86],0,150)
# calculate_TZ(PAX6[86],0,150,PAX6_V)

# plot_single_profile(PAX6[87],0,200)
# calculate_TZ(PAX6[87],0,200,PAX6_V)

# plot_single_profile(PAX6[88],0,350)
# calculate_TZ(PAX6[88],0,350,PAX6_V)

# plot_single_profile(PAX6[89],0,250)
# calculate_TZ(PAX6[89],0,250,PAX6_V)

# plot_single_profile(PAX6[90],0,250)
# calculate_TZ(PAX6[90],0,250,PAX6_V)

# plot_single_profile(PAX6[91],0,250)
# calculate_TZ(PAX6[91],0,250,PAX6_V)

# plot_single_profile(PAX6[92],0,250)
# calculate_TZ(PAX6[92],0,250,PAX6_V)

# plot_single_profile(PAX6[93],0,200)
# calculate_TZ(PAX6[93],0,200,PAX6_V)

# plot_single_profile(PAX6[94],0,250)
# calculate_TZ(PAX6[94],0,250,PAX6_V)

# plot_single_profile(PAX6[95],0,250)
# calculate_TZ(PAX6[95],0,250,PAX6_V)

# plot_single_profile(PAX6[96],0,250)
# calculate_TZ(PAX6[96],0,250,PAX6_V)

# plot_single_profile(PAX6[97],0,250)
# calculate_TZ(PAX6[97],0,250,PAX6_V)

# plot_single_profile(PAX6[98],0,250)
# calculate_TZ(PAX6[98],0,250,PAX6_V)

# plot_single_profile(PAX6[99],0,250)
# calculate_TZ(PAX6[99],0,250,PAX6_V)

# plot_single_profile(PAX6[100],0,250)
# calculate_TZ(PAX6[100],0,250,PAX6_V)

# plot_single_profile(PAX6[101],0,250)
# calculate_TZ(PAX6[101],0,250,PAX6_V)

# plot_single_profile(PAX6[102],0,250)
# calculate_TZ(PAX6[102],0,250,PAX6_V)

# plot_single_profile(PAX6[103],0,250)
# calculate_TZ(PAX6[103],0,250,PAX6_V)

# plot_single_profile(PAX6[104],0,250)
# calculate_TZ(PAX6[104],0,250,PAX6_V)

# plot_single_profile(PAX6[105],0,250)
# calculate_TZ(PAX6[105],0,250,PAX6_V)

# plot_single_profile(PAX6[106],0,250)
# calculate_TZ(PAX6[106],0,250,PAX6_V)

# plot_single_profile(PAX6[107],0,250)
# calculate_TZ(PAX6[107],0,250,PAX6_V)

# plot_single_profile(PAX6[108],0,250)
# calculate_TZ(PAX6[108],0,250,PAX6_V)

# plot_single_profile(PAX6[109],0,250)
# calculate_TZ(PAX6[109],0,250,PAX6_V)

# plot_single_profile(PAX6[110],0,250)
# calculate_TZ(PAX6[110],0,250,PAX6_V)

# plot_single_profile(PAX6[111],0,250)
# calculate_TZ(PAX6[111],0,250,PAX6_V)

# plot_single_profile(PAX6[112],0,250)
# #plot!(legend=:bottomright)
# calculate_TZ(PAX6[112],0,250,PAX6_V)


# # PAX6_D

# plot_single_profile(PAX6[1],150,400)
# calculate_TZ(PAX6[1],150,400,PAX6_D)

# plot_single_profile(PAX6[2],200,400)
# calculate_TZ(PAX6[2],200,400,PAX6_D)

# plot_single_profile(PAX6[3],190,400)
# calculate_TZ(PAX6[3],190,400,PAX6_D)

# plot_single_profile(PAX6[4],230,400)
# calculate_TZ(PAX6[4],230,400,PAX6_D)

# # plot_single_profile(PAX6[5],250,260)
# # calculate_TZ(PAX6[5],225,400,PAX6_D)

# plot_single_profile(PAX6[6],210,400)
# calculate_TZ(PAX6[6],210,400,PAX6_D)

# # plot_single_profile(PAX6[7],150,400)
# # calculate_TZ(PAX6[7],150,300,PAX6_D)

# plot_single_profile(PAX6[8],270,400)
# calculate_TZ(PAX6[8],270,400,PAX6_D)

# plot_single_profile(PAX6[9],200,400)
# calculate_TZ(PAX6[9],200,400,PAX6_D)

# plot_single_profile(PAX6[10],210,400)
# calculate_TZ(PAX6[10],210,400,PAX6_D)

# # plot_single_profile(PAX6[11],230,240)
# # calculate_TZ(PAX6[11],220,300,PAX6_D)

# plot_single_profile(PAX6[12],200,400)
# calculate_TZ(PAX6[12],200,400,PAX6_D)

# plot_single_profile(PAX6[13],200,400)
# calculate_TZ(PAX6[13],200,400,PAX6_D)

# plot_single_profile(PAX6[14],140,400)
# calculate_TZ(PAX6[14],140,400,PAX6_D)

# plot_single_profile(PAX6[15],250,400)
# calculate_TZ(PAX6[15],250,400,PAX6_D)

# # plot_single_profile(PAX6[16],260,310)
# # calculate_TZ(PAX6[16],0,400,PAX6_D)

# plot_single_profile(PAX6[17],150,400)
# calculate_TZ(PAX6[17],150,400,PAX6_D)

# plot_single_profile(PAX6[18],150,400)
# calculate_TZ(PAX6[18],150,400,PAX6_D)

# plot_single_profile(PAX6[19],200,400)
# calculate_TZ(PAX6[19],200,400,PAX6_D)

# plot_single_profile(PAX6[20],200,270)
# calculate_TZ(PAX6[20],200,270,PAX6_D)

# plot_single_profile(PAX6[21],150,400)
# calculate_TZ(PAX6[21],150,400,PAX6_D)

# plot_single_profile(PAX6[22],250,400)
# calculate_TZ(PAX6[22],250,400,PAX6_D)

# # plot_single_profile(PAX6[23],231,400)
# # calculate_TZ(PAX6[23],150,400,PAX6_D)

# plot_single_profile(PAX6[24],280,300)
# calculate_TZ(PAX6[24],260,290,PAX6_D)

# plot_single_profile(PAX6[25],243,300)
# calculate_TZ(PAX6[25],243,300,PAX6_D)

# plot_single_profile(PAX6[26],250,400)
# calculate_TZ(PAX6[26],250,400,PAX6_D)

# plot_single_profile(PAX6[27],200,400)
# calculate_TZ(PAX6[27],200,400,PAX6_D)

# # plot_single_profile(PAX6[28],310,320)
# # calculate_TZ(PAX6[28],250,290,PAX6_D)

# plot_single_profile(PAX6[29],150,400)
# calculate_TZ(PAX6[29],150,400,PAX6_D)

# plot_single_profile(PAX6[30],270,320)
# calculate_TZ(PAX6[30],270,320,PAX6_D)

# plot_single_profile(PAX6[31],150,400)
# calculate_TZ(PAX6[31],150,400,PAX6_D)

# plot_single_profile(PAX6[32],250,300)
# calculate_TZ(PAX6[32],250,300,PAX6_D)

# plot_single_profile(PAX6[33],230,300)
# calculate_TZ(PAX6[33],230,300,PAX6_D)

# # plot_single_profile(PAX6[34],230,300)
# # calculate_TZ(PAX6[34],200,215,PAX6_D)

# plot_single_profile(PAX6[35],270,400)
# calculate_TZ(PAX6[35],270,400,PAX6_D)

# # plot_single_profile(PAX6[36],290,400)
# # calculate_TZ(PAX6[36],0,200,PAX6_D)

# plot_single_profile(PAX6[37],240,400)
# calculate_TZ(PAX6[37],240,400,PAX6_D)

# plot_single_profile(PAX6[38],100,400)
# calculate_TZ(PAX6[38],100,400,PAX6_D)

# plot_single_profile(PAX6[39],150,400)
# calculate_TZ(PAX6[39],150,400,PAX6_D)

# plot_single_profile(PAX6[40],150,400)
# calculate_TZ(PAX6[40],150,400,PAX6_D)

# plot_single_profile(PAX6[41],180,260)
# calculate_TZ(PAX6[41],180,260,PAX6_D)

# plot_single_profile(PAX6[42],160,400)
# calculate_TZ(PAX6[42],160,400,PAX6_D)

# plot_single_profile(PAX6[43],220,400)
# calculate_TZ(PAX6[43],220,400,PAX6_D)

# plot_single_profile(PAX6[44],240,300)
# calculate_TZ(PAX6[44],240,300,PAX6_D)

# plot_single_profile(PAX6[45],230,240)
# calculate_TZ(PAX6[45],230,240,PAX6_D)

# plot_single_profile(PAX6[46],240,300)
# calculate_TZ(PAX6[46],240,300,PAX6_D)

# plot_single_profile(PAX6[47],210,260)
# calculate_TZ(PAX6[47],210,260,PAX6_D)

# plot_single_profile(PAX6[48],250,270)
# calculate_TZ(PAX6[48],250,270,PAX6_D)

# plot_single_profile(PAX6[49],230,400)
# calculate_TZ(PAX6[49],230,400,PAX6_D)

# plot_single_profile(PAX6[50],210,400)
# calculate_TZ(PAX6[50],210,400,PAX6_D)

# plot_single_profile(PAX6[51],150,400)
# calculate_TZ(PAX6[51],150,400,PAX6_D)

# plot_single_profile(PAX6[52],150,400)
# calculate_TZ(PAX6[52],150,400,PAX6_D)

# plot_single_profile(PAX6[53],210,300)
# calculate_TZ(PAX6[53],210,300,PAX6_D)

# plot_single_profile(PAX6[54],200,400)
# calculate_TZ(PAX6[54],200,400,PAX6_D)

# plot_single_profile(PAX6[55],230,400)
# calculate_TZ(PAX6[55],230,400,PAX6_D)

# plot_single_profile(PAX6[56],200,400)
# calculate_TZ(PAX6[56],200,400,PAX6_D)

# plot_single_profile(PAX6[57],180,400)
# calculate_TZ(PAX6[57],180,400,PAX6_D)

# plot_single_profile(PAX6[58],260,400)
# calculate_TZ(PAX6[58],260,400,PAX6_D)

# plot_single_profile(PAX6[59],160,200)
# calculate_TZ(PAX6[59],160,200,PAX6_D)

# plot_single_profile(PAX6[60],150,190)
# calculate_TZ(PAX6[60],150,190,PAX6_D)

# plot_single_profile(PAX6[61],150,400)
# calculate_TZ(PAX6[61],150,400,PAX6_D)

# plot_single_profile(PAX6[62],230,290)
# calculate_TZ(PAX6[62],230,290,PAX6_D)

# plot_single_profile(PAX6[63],110,400)
# calculate_TZ(PAX6[63],110,400,PAX6_D)

# plot_single_profile(PAX6[64],200,400)
# calculate_TZ(PAX6[64],200,400,PAX6_D)

# plot_single_profile(PAX6[65],100,400)
# calculate_TZ(PAX6[65],100,400,PAX6_D)

# plot_single_profile(PAX6[66],100,400)
# calculate_TZ(PAX6[66],100,400,PAX6_D)

# plot_single_profile(PAX6[67],190,400)
# calculate_TZ(PAX6[67],190,400,PAX6_D)

# plot_single_profile(PAX6[68],190,400)
# calculate_TZ(PAX6[68],190,400,PAX6_D)

# plot_single_profile(PAX6[69],160,400)
# calculate_TZ(PAX6[69],160,400,PAX6_D)

# plot_single_profile(PAX6[70],200,400)
# calculate_TZ(PAX6[70],200,400,PAX6_D)

# plot_single_profile(PAX6[71],100,400)
# calculate_TZ(PAX6[71],100,400,PAX6_D)

# plot_single_profile(PAX6[72],184,400)
# calculate_TZ(PAX6[72],184,400,PAX6_D)

# plot_single_profile(PAX6[73],180,400)
# calculate_TZ(PAX6[73],180,400,PAX6_D)

# plot_single_profile(PAX6[74],150,400)
# calculate_TZ(PAX6[74],150,400,PAX6_D)

# plot_single_profile(PAX6[75],150,400)
# calculate_TZ(PAX6[75],150,400,PAX6_D)

# plot_single_profile(PAX6[76],150,400)
# calculate_TZ(PAX6[76],150,400,PAX6_D)

# plot_single_profile(PAX6[77],80,400)
# calculate_TZ(PAX6[77],80,400,PAX6_D)

# plot_single_profile(PAX6[78],110,400)
# calculate_TZ(PAX6[78],110,400,PAX6_D)

# plot_single_profile(PAX6[79],200,400)
# calculate_TZ(PAX6[79],200,400,PAX6_D)

# plot_single_profile(PAX6[80],200,400)
# calculate_TZ(PAX6[80],200,400,PAX6_D)

# plot_single_profile(PAX6[81],100,400)
# calculate_TZ(PAX6[81],100,400,PAX6_D)

# plot_single_profile(PAX6[82],100,400)
# calculate_TZ(PAX6[82],100,400,PAX6_D)

# plot_single_profile(PAX6[83],100,400)
# calculate_TZ(PAX6[83],100,400,PAX6_D)

# # plot_single_profile(PAX6[84],240,400)
# # calculate_TZ(PAX6[84],240,400,PAX6_D)

# plot_single_profile(PAX6[85],100,400)
# calculate_TZ(PAX6[85],100,400,PAX6_D)

# plot_single_profile(PAX6[86],110,400)
# calculate_TZ(PAX6[86],110,400,PAX6_D)

# plot_single_profile(PAX6[87],150,400)
# calculate_TZ(PAX6[87],150,400,PAX6_D)

# plot_single_profile(PAX6[88],150,400)
# calculate_TZ(PAX6[88],150,400,PAX6_D)

# plot_single_profile(PAX6[89],150,400)
# calculate_TZ(PAX6[89],150,400,PAX6_D)

# # plot_single_profile(PAX6[90],300,500)
# # calculate_TZ(PAX6[90],300,500,PAX6_D)

# plot_single_profile(PAX6[91],200,400)
# calculate_TZ(PAX6[91],200,400,PAX6_D)

# plot_single_profile(PAX6[92],240,410)
# calculate_TZ(PAX6[92],240,410,PAX6_D)

# plot_single_profile(PAX6[93],100,400)
# calculate_TZ(PAX6[93],100,400,PAX6_D)

# plot_single_profile(PAX6[94],100,420)
# calculate_TZ(PAX6[94],100,420,PAX6_D)

# plot_single_profile(PAX6[95],100,400)
# calculate_TZ(PAX6[95],100,400,PAX6_D)

# plot_single_profile(PAX6[96],100,400)
# calculate_TZ(PAX6[96],100,400,PAX6_D)

# plot_single_profile(PAX6[97],100,400)
# calculate_TZ(PAX6[97],100,400,PAX6_D)

# plot_single_profile(PAX6[98],100,400)
# calculate_TZ(PAX6[98],100,400,PAX6_D)

# plot_single_profile(PAX6[99],200,400)
# calculate_TZ(PAX6[99],200,400,PAX6_D)

# # plot_single_profile(PAX6[100],150,500)
# # calculate_TZ(PAX6[100],150,500,PAX6_D)

# plot_single_profile(PAX6[101],100,400)
# calculate_TZ(PAX6[101],100,400,PAX6_D)

# plot_single_profile(PAX6[102],200,400)
# calculate_TZ(PAX6[102],200,400,PAX6_D)

# plot_single_profile(PAX6[103],100,400)
# calculate_TZ(PAX6[103],100,400,PAX6_D)

# plot_single_profile(PAX6[104],100,400)
# calculate_TZ(PAX6[104],100,400,PAX6_D)

# plot_single_profile(PAX6[105],150,400)
# calculate_TZ(PAX6[105],150,400,PAX6_D)

# plot_single_profile(PAX6[106],100,400)
# calculate_TZ(PAX6[106],100,400,PAX6_D)

# plot_single_profile(PAX6[107],100,400)
# calculate_TZ(PAX6[107],100,400,PAX6_D)

# plot_single_profile(PAX6[108],100,400)
# calculate_TZ(PAX6[108],100,400,PAX6_D)

# plot_single_profile(PAX6[109],100,400)
# calculate_TZ(PAX6[109],100,400,PAX6_D)

# plot_single_profile(PAX6[110],100,420)
# calculate_TZ(PAX6[109],100,420,PAX6_D)

# plot_single_profile(PAX6[111],200,400)
# calculate_TZ(PAX6[111],200,400,PAX6_D)

# plot_single_profile(PAX6[112],100,410)
# calculate_TZ(PAX6[112],100,410,PAX6_D)

# # end of manual procedure


# # # prepare output data
# NKX22_V=prep_df(NKX22_V,:NKX22_V);
# NKX22_D=prep_df(NKX22_D,:NKX22_D)
# NKX61_V=prep_df(NKX61_V,:NKX61_V);
# NKX61_D=prep_df(NKX61_D,:NKX61_D);
# OLIG2_V=prep_df(OLIG2_V,:OLIG2_V);
# OLIG2_D=prep_df(OLIG2_D,:OLIG2_D);
# OLIG3_V=prep_df(OLIG3_V,:OLIG3_V);
# PAX6_V=prep_df(PAX6_V,:PAX6_V);
# PAX6_D=prep_df(PAX6_D,:PAX6_D);

# df_boundaries = vcat(NKX22_V,NKX22_D,NKX61_V,NKX61_D,OLIG2_V,OLIG2_D,OLIG3_V,PAX6_V,PAX6_D) # concatenate output
# df_boundaries.tz = abs.(df_boundaries.tz_upper.-df_boundaries.tz_lower) # calculate transition zones
# df_boundaries = mapcols(col -> replace!(col, Inf=>0), df_boundaries) # in-place
# gDF = groupby(df_boundaries,:domain) # group df by domain
# DF = combine(gDF, :tz => mean)
# DFtz = innerjoin(df_boundaries,DF,on=:domain)
# DFtz.tz = round.(DFtz.tz,digits = 1)

# # # For Romans precision project
# # DFtz_sel = select!(DFtz, ["L_T","boundary","tz","domain","sample" ])
# # DFtz_sel = DFtz_sel[DFtz_sel.domain .== "NKX61_D",:]
# # CSV.write(raw".\\NKX61_D.csv", DFtz_sel,delim=';')

# CSV.write(raw".\\TZ_medianROIwidth_ss.csv", DFtz,delim=';')

DFtz = CSV.File(raw".\\TZ_medianROIwidth.csv") |> DataFrame

# Define bins
hundredmicrons = collect(round(minimum(DFtz.L_T)):100:round(maximum(DFtz.L_T)))
fiftymicrons = collect(round(minimum(DFtz.L_T)):50:round(maximum(DFtz.L_T)))
twentyfivemicrons = collect(round(minimum(DFtz.L_T)):25:round(maximum(DFtz.L_T)))
fifteenmicrons = collect(round(minimum(DFtz.L_T)):15:round(maximum(DFtz.L_T)))
tenmicrons = collect(round(minimum(DFtz.L_T)):10:round(maximum(DFtz.L_T)))
fivemicrons = collect(round(minimum(DFtz.L_T)):5:round(maximum(DFtz.L_T)))



# Now change domain boundary tickers
DFtz.domain = switch2int(DFtz.domain)


# get binned+bootstrapped readout/boundary positions
stdDF_hundredmicrons = getstdDF(hundredmicrons)
stdDF_fiftymicrons = getstdDF(fiftymicrons)

# # For Romans precision project
# stdDF_fiftymicrons_NKX61_D = stdDF_fiftymicrons[stdDF_fiftymicrons.domain .== "NKX61_D",:]
# CSV.write(raw".\\NKX61_D_binned_50microns.csv", stdDF_fiftymicrons_NKX61_D,delim=';')

stdDF_twentyfivemicrons = getstdDF(twentyfivemicrons)
stdDF_fifteenmicrons = getstdDF(fifteenmicrons)
stdDF_tenmicrons = getstdDF(tenmicrons)
stdDF_fivemicrons = getstdDF(fivemicrons)
# get binned+bootstrapped transition widths
meantzDF_hundredmicrons = getmeantzDF(hundredmicrons)
meantzDF_fiftymicrons = getmeantzDF(fiftymicrons)
meantzDF_twentyfivemicrons = getmeantzDF(twentyfivemicrons)
meantzDF_fifteenmicrons = getmeantzDF(fifteenmicrons)
meantzDF_tenmicrons = getmeantzDF(tenmicrons)
meantzDF_fivemicrons = getmeantzDF(fivemicrons)



# main figure

p1 = Plots.scatter(DFtz.L_T,DFtz.boundary,group=DFtz.domain,
            xlabel="NT length [μm]",
            ylabel="readout position [μm]",
            yticks=[0,200,400,600],
            xlims=(150,700),
            ylims=(-10,600),
            color_palette=custom_colors,
            labels = permutedims(tickers),
            legend=(0.12,0.95)
            )
Plots.plot!(DFtz.L_T,DFtz.L_T,color=:gray,label="NT length")

p2 = Plots.scatter(stdDF_fivemicrons.bins,stdDF_fivemicrons.boundary_std,yerror=stdDF_fivemicrons.boundary_se,group=stdDF_fivemicrons.domain,
        xlabel="NT length [μm]",
        ylabel="positional error [μm]",
        xlims=(150,700),
        yticks=[0,20,40,60,80,90],
        ylims=(-1,90),
        legend = false,
        color_palette=custom_colors,
        labels = permutedims(tickers)
        )
annotate!(375,90,text("bin size: 5μm",10))

p3 = Plots.scatter(stdDF_fivemicrons.boundary_mean,stdDF_fivemicrons.boundary_std,group=stdDF_fivemicrons.domain,
        xlabel="readout position [μm]",
        ylabel="positional error [μm]",
        ylims=(-1,60),
        xlims=(-25,600),
        legend = false,
        color_palette=custom_colors,
        labels = permutedims(tickers)
        )
annotate!(300,60,text("bin size: 5μm",10))

p4 = Plots.scatter(DFtz.L_T,DFtz.tz,group=DFtz.domain,
        line=(:spline,2),
        yaxis=:log,
        ylims=(0.08,1000),
        yticks=[0.1,1.,10.,100.,1000.],
            xlabel="NT length [μm]",
                ylabel="transition width [μm]",
                xlims=(150,700),
                legend=false,
                color_palette=custom_colors,
                labels = permutedims(tickers)
        )
Plots.plot!(DFtz.L_T,DFtz.L_T,color=:gray)

p5 = Plots.scatter(DFtz.boundary,DFtz.tz,group=DFtz.domain,
        line=(:spline,2),
        yaxis=:log,
        xlims=(-10,600),
        ylims=(0.08,1000),
        yticks=[0.1,1.,10.,100.,1000.],
        xlabel="readout position [μm]",
                ylabel="transition width [μm]",
                legend=false,
                color_palette=custom_colors,
                labels = permutedims(tickers)
        )

# plot panel 6 w/o PAX6
meantzDF_fivemicrons_no_PAX6 = meantzDF_fivemicrons[(meantzDF_fivemicrons.domain .!= 7) .& (meantzDF_fivemicrons.domain .!= 8),:]
meantzDF_fivemicrons_no_PAX6 = filter(:boundary_se => x -> !any(f -> f(x), (ismissing, isnothing, isnan)), meantzDF_fivemicrons_no_PAX6)

p6_no_Pax6= Plots.scatter(meantzDF_fivemicrons_no_PAX6.boundary_std, meantzDF_fivemicrons_no_PAX6.tz_mean,group=meantzDF_fivemicrons_no_PAX6.domain,
        ylims=(0.1,50),
        xlims=(-0.5,30),
        xlabel="positional error [μm]",
        ylabel="transition width [μm]",
        color_palette=[greens[3],greens[7],:magenta,:red,blues[3],blues[7],:brown],
        labels = permutedims(["NKX22_V","NKX22_D","OLIG2_V","OLIG2_D","NKX61_V","NKX61_D","OLIG3_V"]), legend=(0.84,0.83)
        )
annotate!(20,53,text("bin size: 5μm",10))
Plots.plot!(meantzDF_fivemicrons_no_PAX6.boundary_std,2*meantzDF_fivemicrons_no_PAX6.boundary_std,color=:black,label="2* pos. error")




# panel 6 with Pax6 as supp. figure
meantzDF_fivemicrons = filter(:boundary_se => x -> !any(f -> f(x), (ismissing, isnothing, isnan)), meantzDF_fivemicrons)

p6= Plots.scatter(meantzDF_fivemicrons.boundary_std, meantzDF_fivemicrons.tz_mean,group=meantzDF_fivemicrons.domain,
        ylims=(0.1,50),
        xlims=(-0.5,40),
        xlabel="positional error [μm]",
        ylabel="transition width [μm]",
        #smooth=true,
        color_palette=custom_colors,
        labels = permutedims(tickers),
        legend = false,
        #legend=:topright,
        aspectratio=0.8 ,legendfontsize=5,
        xguidefontsize=13,yguidefontsize=13, margin=5Plots.mm,guidefont=font(10)
        )
annotate!(20,53,text("bin size: 5μm",10))
Plots.plot!(meantzDF_fivemicrons.boundary_std,2*meantzDF_fivemicrons.boundary_std,color=:black,label="2* pos. error")



# combine all panels
p = Plots.plot(p1,p2,p3,p4,p5,p6,
layout=@layout([a b c; d e f]),size=(950,650),
title=["A" "B" "C" "D" "E" "F" "G"], titleloc=:left,legendfontsize=5,
xguidefontsize=13,yguidefontsize=13, margin=5Plots.mm,guidefont=font(10))

Plots.savefig(p,"TZs.svg")


Plots.savefig(p6,"tw_vs_pe.svg")


r²=round(r2(lm(@formula(tz_mean ~ boundary_std), meantzDF_fivemicrons)),digits=2)


𝜌=round(cor(meantzDF_fivemicrons.boundary_std, meantzDF_fivemicrons.tz_mean),digits=2)

Plots.scatter(meantzDF_fivemicrons.boundary_std, meantzDF_fivemicrons.tz_mean,#group=meantzDF_fivemicrons.domain,
        ylims=(0.1,50),
        xlims=(-0.5,40),
        xlabel="positional error [μm]",
        ylabel="transition width [μm]",
        smooth=true,
        legend=false,aspectratio=0.8 ,legendfontsize=5,
        xguidefontsize=13,yguidefontsize=13, margin=5Plots.mm,guidefont=font(10),
        annotation=(20,52,
        latexstring("\$\\rho=$(𝜌), r^2=$(r²)\$"))
        )

#
r²=round(r2(lm(@formula(tz_mean ~ boundary_std), meantzDF_fivemicrons_no_PAX6)),digits=2)


𝜌=round(cor(meantzDF_fivemicrons_no_PAX6.boundary_std, meantzDF_fivemicrons_no_PAX6.tz_mean),digits=2)

Plots.scatter(meantzDF_fivemicrons_no_PAX6.boundary_std, meantzDF_fivemicrons_no_PAX6.tz_mean,#group=meantzDF_fivemicrons.domain,
        ylims=(0.1,50),
        xlims=(-0.5,40),
        xlabel="positional error [μm]",
        ylabel="transition width [μm]",
        smooth=true,
        legend=false,aspectratio=0.8 ,legendfontsize=5,
        xguidefontsize=13,yguidefontsize=13, margin=5Plots.mm,guidefont=font(10),
        annotation=(20,52,
        latexstring("\$\\rho=$(𝜌), r^2=$(r²)\$"))
        )


# Transition width for different choices of bin size

bins_tz = vcat(meantzDF_hundredmicrons, meantzDF_fiftymicrons, meantzDF_twentyfivemicrons, meantzDF_fifteenmicrons, meantzDF_tenmicrons, meantzDF_fivemicrons)

tp1 = plot_L_T_vs_tz_mean(meantzDF_hundredmicrons,(0.5,0.95))
tp2 = plot_L_T_vs_tz_mean(meantzDF_fiftymicrons,false)
tp3 = plot_L_T_vs_tz_mean(meantzDF_twentyfivemicrons,false)
tp4 = plot_L_T_vs_tz_mean(meantzDF_fifteenmicrons,false)
tp5 = plot_L_T_vs_tz_mean(meantzDF_tenmicrons,false)
tp6 = plot_L_T_vs_tz_mean(meantzDF_fivemicrons,false)

sp = Plots.plot(tp1,tp2,tp3,tp4,tp5,tp6,
layout=@layout([a b c; d e f]),size=(950,650),
title=["bin size: 100μm" "bin size: 50μm" "bin size: 25μm" "bin size: 15μm" "bin size: 10μm" "bin size: 5μm"], titleloc=:center,legendfontsize=6,
xguidefontsize=13,yguidefontsize=13, margin=5Plots.mm,guidefont=font(10),titlefontsize=10)

Plots.savefig(sp,"var_bin_size.svg")



# Positional error for different choices of bin size

DF_hundredmicrons = getTimeAv(hundredmicrons)
DF_hundredmicrons[!,:intsize] .= 100
DF_fiftymicrons = getTimeAv(fiftymicrons)
DF_fiftymicrons[!,:intsize] .= 50
DF_twentyfivemicrons = getTimeAv(twentyfivemicrons)
DF_twentyfivemicrons[!,:intsize] .= 25
DF_fifteenmicrons = getTimeAv(fifteenmicrons)
DF_fifteenmicrons[!,:intsize] .= 15
DF_tenmicrons = getTimeAv(tenmicrons)
DF_tenmicrons[!,:intsize] .= 10
DF_fivemicrons = getTimeAv(fivemicrons)
DF_fivemicrons[!,:intsize] .= 5


function joingroupmean(DF,bin_size)
    DF[:,:nbins] .= length(unique(DF.bins))
    DF[:,:bin_size] .= bin_size
    #DF
    innerjoin(combine(groupby(DF,:domain), :boundary_std => mean),DF,on=:domain)
end

stdDF_hundredmicrons = joingroupmean(stdDF_hundredmicrons,100)
stdDF_fiftymicrons = joingroupmean(stdDF_fiftymicrons,50)
stdDF_twentyfivemicrons = joingroupmean(stdDF_twentyfivemicrons,25)
stdDF_fifteenmicrons = joingroupmean(stdDF_fifteenmicrons,15)
stdDF_tenmicrons = joingroupmean(stdDF_tenmicrons,10)
stdDF_fivemicrons = joingroupmean(stdDF_fivemicrons,5)


sp7DF = vcat(DF_hundredmicrons,DF_fiftymicrons,DF_twentyfivemicrons,DF_fifteenmicrons,DF_fivemicrons)
sp7DF.domain = switch2str(sp7DF.domain)
sp8DF_1dia = vcat(getFraction(stdDF_hundredmicrons,4.9),getFraction(stdDF_fiftymicrons,4.9),getFraction(stdDF_twentyfivemicrons,4.9),getFraction(stdDF_fifteenmicrons,4.9),getFraction(stdDF_tenmicrons,4.9),getFraction(stdDF_fivemicrons,4.9))
sp8DF_1dia.domain = switch2str(sp8DF_1dia.domain)
sp8DF_2dia = vcat(getFraction(stdDF_hundredmicrons,9.8),getFraction(stdDF_fiftymicrons,9.8),getFraction(stdDF_twentyfivemicrons,9.8),getFraction(stdDF_fifteenmicrons,9.8),getFraction(stdDF_tenmicrons,9.8),getFraction(stdDF_fivemicrons,9.8))
sp8DF_2dia.domain = switch2str(sp8DF_2dia.domain)

sp1 = Plots.scatter(stdDF_hundredmicrons.bins,stdDF_hundredmicrons.boundary_std,group=stdDF_hundredmicrons.domain,
        xlabel="NT length [μm]",
        ylabel="positional error [μm]",
        legend = (0.95,0.95),
        color_palette=custom_colors,
        labels = permutedims(tickers),
        xlims=(100,700),ylims=(-1,50)
        )
        annotate!(375,53,text("bin size: 100μm",10))
#
sp2 = Plots.scatter(stdDF_fiftymicrons.bins,stdDF_fiftymicrons.boundary_std,group=stdDF_fiftymicrons.domain,
        xlabel="NT length [μm]",
        ylabel="positional error [μm]",
        legend = false,
        color_palette=custom_colors,
        labels = permutedims(tickers),
        xlims=(100,700),ylims=(-1,50)
        )
        annotate!(375,53,text("bin size: 50μm",10))
#
sp3 = Plots.scatter(stdDF_twentyfivemicrons.bins,stdDF_twentyfivemicrons.boundary_std,group=stdDF_twentyfivemicrons.domain,
        xlabel="NT length [μm]",
        ylabel="positional error [μm]",
        legend = false,
        color_palette=custom_colors,
        labels = permutedims(tickers),
        xlims=(100,700),ylims=(-1,50)
        )
        annotate!(375,53,text("bin size: 25μm",10))
#
sp4 = Plots.scatter(stdDF_fifteenmicrons.bins,stdDF_fifteenmicrons.boundary_std,group=stdDF_fifteenmicrons.domain,
        xlabel=xlabel="NT length [μm]",
        ylabel="positional error [μm]",
        legend = false,
        color_palette=custom_colors,
        labels = permutedims(tickers),
        xlims=(100,700),ylims=(-1,50)
        )
        annotate!(375,53,text("bin size: 15μm",10))
#
sp5 = Plots.scatter(stdDF_tenmicrons.bins,stdDF_tenmicrons.boundary_std,group=stdDF_tenmicrons.domain,
        xlabel="NT length [μm]",
        ylabel="positional error [μm]",
        legend = false,
        color_palette=custom_colors,
        labels = permutedims(tickers),
        xlims=(100,700),ylims=(-1,50)
        )
        annotate!(375,53,text("bin size: 10μm",10))

#
sp6 = Plots.scatter(stdDF_fivemicrons.bins,stdDF_fivemicrons.boundary_std,group=stdDF_fivemicrons.domain,
        xlabel="NT length [μm]",
        ylabel="positional error [μm]",
        legend = false,
        color_palette=custom_colors,
        labels = permutedims(tickers),
        xlims=(100,700),ylims=(-1,60)
        )
        annotate!(375,63,text("bin size: 5μm",10))

sp7 = Plots.plot(sp7DF.intsize,sp7DF.boundary_std_mean,group=sp7DF.domain,
        xlabel="bin size [μm]",
        ylabel="average positional error [μm]",
        legend = false,
        color_palette=custom_colors,
        xlims=(0,102),ylims=(0,40),
        markershape = :circle
        )


sp8 = Plots.plot(sp8DF_1dia.bin_size,sp8DF_1dia.fraction*100,group=sp8DF_1dia.domain,
                 legend=false,
                 #xlims=(0,102),ylims=(-2,120),
                 markershape = :circle,xlabel="bin size [μm]",ylabel="pos. err. ≤ 4.9μm [%]",
                 color_palette=custom_colors,
                 labels = permutedims(tickers)
                 )
sp9 = Plots.plot(sp8DF_2dia.bin_size,sp8DF_2dia.fraction*100,group=sp8DF_2dia.domain,
                 legend=false,
                 #xlims=(0,102),ylims=(75,85),
                 markershape = :circle,xlabel="bin size [μm]",ylabel="pos. err. ≤ 9.8μm [%]",
                 color_palette=custom_colors,
                 labels = permutedims(tickers)
                 )


blankplot = Plots.plot(legend=false,foreground_color_subplot=:white)



sp = Plots.plot(sp1,sp2,sp3,sp4,sp5,sp6,sp7,sp8,sp9,
    layout=Plots.grid(3,3),title=["A" "B" "C" "D" "E" "F" "G" "H" "I"],
    size=(950,950),titleloc=:left,legendfontsize=5,
xguidefontsize=13,yguidefontsize=13, margin=5Plots.mm,guidefont=font(10))


Plots.savefig(sp,"pos_err_bin_size_var.svg")



mDF = vcat(stdDF_hundredmicrons,stdDF_fiftymicrons,stdDF_twentyfivemicrons,stdDF_fifteenmicrons,stdDF_tenmicrons,stdDF_fivemicrons)
mDF =  unique( select!(mDF, ["domain","bin_size","n_measurements"]))
combine(groupby(mDF,:bin_size),:n_measurements => mean)
mDF =  combine(groupby(mDF,:bin_size), [:n_measurements] => ( (x) -> (nm_mean=mean(x), nm_std=std(x)) ) => AsTable)



#
mp = Plots.plot(mDF.bin_size,mDF.nm_mean, yerror=mDF.nm_std,
markershape = :circle,
    xlabel="bin size [μm]",
        ylabel="measurements per bin",
        xlims=(0,101),ylims=(0,300),
        legend=:false,aspect_ratio=1//3
)

Plots.savefig(mp,"meas_per_bin_vs_bin_size.svg")